function setRusMonths(target) {
    var months = ['Нулября', 'Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
    target.find('option').each(function() {
        $(this).html(months[$(this).val()]);
    });
}