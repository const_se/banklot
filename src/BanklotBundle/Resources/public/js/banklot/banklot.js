!function($) {
    $.fn.initHeaderMenu = function() {
        this.find('.notices > a').click(function(e) {
            $(this).toggleSubmenu();
            e.preventDefault();

            return false;
        });

        this.find('.user > a').click(function(e) {
            $(this).toggleSubmenu();
            e.preventDefault();

            return false;
        });
    };

    $.fn.setRusMonths = function() {
        var months = ['Нулября', 'Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
        this.each(function() {
            $(this).find('option').each(function() {
                $(this).html(months[$(this).val()]);
            });
        });
    };

    $.fn.toggleSubmenu = function() {
        var submenu = this.parent().find('.submenu');

        if (submenu.is(':visible')) submenu.stop(true, true).fadeOut(250);
        else submenu.stop(true, true).fadeIn(250);

        this.parent().siblings().find('.submenu:visible').stop(true, true).fadeOut(250);
    }
}(jQuery);

$(document).ready(function() {
    $('header.main').initHeaderMenu();
});