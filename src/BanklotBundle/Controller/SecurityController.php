<?php

namespace BanklotBundle\Controller;

use BanklotBundle\Entity\AdminUser;
use BanklotBundle\Entity\Role;
use BanklotBundle\Entity\User;
use BanklotBundle\Form\Type\Security\AdminLoginType;
use BanklotBundle\Form\Type\Security\LoginType;
use BanklotBundle\Form\Type\Security\ProfileType;
use BanklotBundle\Form\Type\Security\Registration1Type;
use BanklotBundle\Form\Type\Security\Registration2Type;
use BanklotBundle\Form\Type\Security\Registration3Type;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Cfg;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class SecurityController extends InitializableController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/denied", name = "security_access_denied")
     */
    public function accessDeniedAction()
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($this->authChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            switch ($user->getStatus()) {
                case User::REGISTRATION_1: return $this->redirectToRoute('security_registration_2');
                case User::REGISTRATION_2: return $this->redirectToRoute('security_registration_3');
            }
        }

        /** @var \BanklotBundle\Entity\Repository\ConfigRepository $configRepository */
        $configRepository = $this->getRepository('Config');

        if (is_null($user->getCertificate())) $allowedBalance = $configRepository->get('allowed_balance', 50000);
        else $allowedBalance = $configRepository->get('allowed_certificate_balance', 30000);

        return $this->render('BanklotBundle:Security:access_denied.html.twig', array(
            'allowed_balance' => $allowedBalance
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/admin/login", name = "security_admin_login")
     */
    public function adminLoginAction()
    {
        if ($this->authChecker->isGranted(Role::AUTHENTICATED_FULLY))
            return $this->redirectToRoute('admin_index');

        /** @var \Symfony\Component\Security\Http\Authentication\AuthenticationUtils $authUtils */
        $authUtils = $this->get('security.authentication_utils');
        $error = $authUtils->getLastAuthenticationError();

        $form = $this->createForm(new AdminLoginType(), new AdminUser());
        $form->get('username')->setData($authUtils->getLastUsername());

        if ($error) {
            $form->get('username')->addError(new FormError('Неправильно введен логин...'));
            $form->get('password')->addError(new FormError('...или пароль'));
        }

        return $this->render('BanklotBundle:Security:admin_login.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Cfg\Route("/admin/login-check", name = "security_admin_login_check")
     */
    public function adminLoginCheckAction()
    {
        throw $this->createNotFoundException();
    }

    /**
     * @Cfg\Route("/admin/logout", name = "security_admin_logout")
     */
    public function adminLogoutAction()
    {
        throw $this->createNotFoundException();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/login", name = "security_login")
     */
    public function loginAction()
    {
        if ($this->authChecker->isGranted(Role::AUTHENTICATED_FULLY))
            return $this->redirectToRoute('lots_all');

        /** @var \Symfony\Component\Security\Http\Authentication\AuthenticationUtils $authUtils */
        $authUtils = $this->get('security.authentication_utils');
        $error = $authUtils->getLastAuthenticationError();

        $form = $this->createForm(new LoginType(), new User());
        $form->get('username')->setData($authUtils->getLastUsername());

        if ($error) {
            $form->get('username')->addError(new FormError('Неправильно введен логин...'));
            $form->get('password')->addError(new FormError('...или пароль'));
        }

        return $this->render('BanklotBundle:Security:login.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Cfg\Route("/login-check", name = "security_login_check")
     */
    public function loginCheckAction()
    {
        throw $this->createNotFoundException();
    }

    /**
     * @Cfg\Route("/logout", name = "security_logout")
     */
    public function logoutAction()
    {
        throw $this->createNotFoundException();
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/profile", name = "security_profile")
     */
    public function profileAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $form = $this->createForm(new ProfileType(), $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $valid = true;
            /** @var \BanklotBundle\Entity\Repository\UserRepository $userRepository */
            $userRepository = $this->getRepository('User');
            $sames = $userRepository->findSamesCount($user);

            if ($sames) {
                $form->get('nickname')->addError(new FormError('Такой никнейм уже занят'));
                $valid = false;
            }

            /** @var \BanklotBundle\Entity\UploadedFile $passport */
            $passport = $form->get('passportPhoto')->getData();

            if (!is_null($passport)) {
                $passport->upload('uploads/secured/users/' . $user->getId());
                $this->manager->persist($passport);
                $user->addFile($passport);
            }

            /** @var \BanklotBundle\Entity\UploadedFile $inn */
            $inn = $form->get('INNPhoto')->getData();

            if (!is_null($inn)) {
                $inn->upload('uploads/secured/users/' . $user->getId());
                $this->manager->persist($inn);
                $user->addFile($inn);
            }

            if ($valid) {
                $this->manager->persist($user);
                $this->manager->flush();

                return $this->redirectToRoute('lots_all');
            }
        }

        return $this->render('BanklotBundle:Security:profile.html.twig', array('form' => $form->createView()));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/profile-view", name = "security_profile_view")
     */
    public function profileViewAction()
    {
        return $this->render('BanklotBundle:Security:profile_view.html.twig');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/registration/step-1", name = "security_registration_1")
     */
    public function registration1Action(Request $request)
    {
        if ($this->authChecker->isGranted(Role::AUTHENTICATED_FULLY))
            return $this->redirectToRoute('lots_all');

        $user = new User();
        $form = $this->createForm(new Registration1Type(), $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $valid = true;
            /** @var \BanklotBundle\Entity\Repository\UserRepository $userRepository */
            $userRepository = $this->getRepository('User');
            $sames = $userRepository->findSamesCount($user);

            if ($sames) {
                $form->get('username')->addError(new FormError('Такой пользователь уже существует'));
                $valid = false;
            }

            if ($valid) {
                $ref = $request->query->get('ref', null);

                if (!is_null($ref)) {
                    /** @var User $ref */
                    $ref = $userRepository->find($ref);

                    if (!is_null($ref)) $user->setInvitedBy($ref);
                }

                /** @var \BanklotBundle\Entity\Repository\RoleRepository $roleRepository */
                $roleRepository = $this->getRepository('Role');
                /** @var \Symfony\Component\Security\Core\Encoder\UserPasswordEncoder $encoder */
                $encoder = $this->get('security.password_encoder');
                $password = $form->get('password')->getData();
                $user->setSalt(User::generateSalt())
                    ->setPassword($encoder->encodePassword($user, $password))
                    ->addRole($roleRepository->findRole(Role::USER));
                $this->manager->persist($user);
                $this->manager->flush();

                /** @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage $tokenStorage */
                $tokenStorage = $this->get('security.token_storage');
                $session = $request->getSession();
                $token = new UsernamePasswordToken($user, null, 'lots', $user->getRoles());
                $tokenStorage->setToken($token);
                $event = new InteractiveLoginEvent($request, $token);
                $this->get('event_dispatcher')->dispatch('security.interactive_login', $event);
                $session->set('_security_secured', serialize($token));
                $session->save();

                $message = \Swift_Message::newInstance()
                    ->setSubject('Регистрация в Banklot')
                    ->setFrom('noreply@banklot.org')
                    ->setTo($user->getUsername())
                    ->setBody(
                        $this->renderView('BanklotBundle:Security:registration_mail.html.twig', array('username' => $user->getUsername(), 'password' => $password)),
                        'text/html'
                    );
                $this->get('mailer')->send($message);

                return $this->redirectToRoute('security_registration_2');
            }
        }

        return $this->render('BanklotBundle:Security:registration1.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/registration/step-2", name = "security_registration_2")
     */
    public function registration2Action(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$this->authChecker->isGranted(Role::AUTHENTICATED_FULLY))
            return $this->redirectToRoute('security_login');
        elseif ($user->getStatus() == User::REGISTERED)
            return $this->redirectToRoute('lots_all');

        $form = $this->createForm(new Registration2Type(), $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $valid = true;
            /** @var \BanklotBundle\Entity\Repository\UserRepository $userRepository */
            $userRepository = $this->getRepository('User');
            $sames = $userRepository->findSamesCount($user);

            if ($sames) {
                $form->get('nickname')->addError(new FormError('Такой никнейм уже занят'));
                $valid = false;
            }

            if ($valid) {
                $user->setStatus($user->getStatus() < User::REGISTRATION_2 ? User::REGISTRATION_2 : $user->getStatus());
                $this->manager->persist($user);
                $this->manager->flush();

                return $this->redirectToRoute('security_registration_3');
            }
        }

        return $this->render('BanklotBundle:Security:registration2.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/registration/step-3", name = "security_registration_3")
     */
    public function registration3Action(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$this->authChecker->isGranted(Role::AUTHENTICATED_FULLY))
            return $this->redirectToRoute('security_login');
        elseif ($user->getStatus() < User::REGISTRATION_2)
            return $this->redirectToRoute('security_registration_2');
        elseif ($user->getStatus() == User::REGISTERED)
            return $this->redirectToRoute('lots_all');

        $form = $this->createForm(new Registration3Type(), $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $valid = true;
            /** @var \BanklotBundle\Entity\UploadedFile $passport */
            $passport = $form->get('passportPhoto')->getData();

            if (is_null($passport)) {
                $form->get('passportPhoto')->addError(new FormError('Вы не приложили фото/скан паспорта'));
                $valid = false;
            }

            if ($valid) {
                $passport->upload('uploads/secured/users/' . $user->getId());
                $this->manager->persist($passport);
            }

            /** @var \BanklotBundle\Entity\UploadedFile $inn */
            $inn = $form->get('INNPhoto')->getData();

            if (is_null($inn)) {
                $form->get('INNPhoto')->addError(new FormError('Вы не приложили фото/скан ИНН'));
                $valid = false;
            }

            if ($valid) {
                $inn->upload('uploads/secured/users/' . $user->getId());
                $this->manager->persist($inn);
            }

            $cert = $form->get('certificate')->getData();

            if (!empty($cert)) {
                /** @var \BanklotBundle\Entity\Repository\CertificateRepository $certRepository */
                $certRepository = $this->getRepository('Certificate');
                $cert = $certRepository->findCertificate($user->getName(), $cert);

                if (is_null($cert)) {
                    $form->get('certificate')->addError(new FormError('Неправильный номер сертификата'));
                    $valid = false;
                }
            }

            if ($valid) {
                $user->setStatus($user->getStatus() < User::REGISTERED ? User::REGISTERED : $user->getStatus())
                    ->addFile($passport)
                    ->addFile($inn);
                $this->manager->persist($user);
                $this->manager->flush();

                return $this->redirectToRoute('security_registration_4');
            }
        }

        return $this->render('BanklotBundle:Security:registration3.html.twig', array('form' => $form->createView()));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Cfg\Route("/registration/step-4", name = "security_registration_4")
     */
    public function registration4Action()
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$this->authChecker->isGranted(Role::AUTHENTICATED_FULLY))
            return $this->redirectToRoute('security_login');
        elseif ($user->getStatus() < User::REGISTRATION_2)
            return $this->redirectToRoute('security_registration_2');
        elseif ($user->getStatus() < User::REGISTERED)
            return $this->redirectToRoute('security_registration_3');

        return $this->redirectToRoute('security_access_denied');
    }
} 