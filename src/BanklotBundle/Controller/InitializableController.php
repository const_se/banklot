<?php

namespace BanklotBundle\Controller;

use BanklotBundle\Entity\Role;
use BanklotBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

abstract class InitializableController extends Controller
{
    /** @var \Symfony\Component\Security\Core\Authorization\AuthorizationChecker */
    protected $authChecker;
    /** @var \Doctrine\ORM\EntityManager */
    protected $manager;
    /** @var \BanklotBundle\Entity\Notice[] */
    protected $notices;
    /** @var \Doctrine\ORM\EntityRepository[] */
    protected $repositories;

    /**
     * @return bool
     */
    public function checkAccess()
    {
        /** @var \BanklotBundle\Entity\User $user */
        $user = $this->getUser();

        if ($this->authChecker->isGranted(Role::ADMIN)) return true;

        if ($this->authChecker->isGranted(Role::AUTHENTICATED_FULLY) && $user->getStatus() == User::REGISTERED) {
            $balance = $user->getBalance();
            $certificate = $user->getCertificate();
            /** @var \BanklotBundle\Entity\Repository\ConfigRepository $configRepository */
            $configRepository = $this->getRepository('Config');
            $allowedBalance = $configRepository->get('allowed_balance', 50000);
            $allowedCertificateBalance = $configRepository->get('allowed_certificate_balance', 30000);

            return (is_null($certificate) && $balance >= $allowedBalance)
                || (!is_null($certificate) && $balance >= $allowedCertificateBalance);
        }

        return false;
    }

    /**
     * @param $entity
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository($entity)
    {
        if (!array_key_exists($entity, $this->repositories))
            $this->repositories[$entity] = $this->manager->getRepository('BanklotBundle:' . $entity);

        return $this->repositories[$entity];
    }

    public function initialize()
    {
        $this->authChecker = $this->get('security.authorization_checker');
        $this->manager = $this->getDoctrine()->getManager();
        $this->notices = array();
        $this->repositories = array();

        $this->getNotices();
    }

    public function getNotices()
    {
        if ($this->authChecker->isGranted(ROle::AUTHENTICATED_FULLY))
            $this->notices = $this->getRepository('Notice')->createQueryBuilder('n')
                ->where('n.toUser = :user')
                ->andWhere('n.viewed = :viewed')
                ->setParameters(array('user' => $this->getUser(), 'viewed' => false))
                ->orderBy('n.createdAt', 'DESC')
                ->getQuery()
                ->getResult();
    }

    /**
     * @param string $view
     * @param array $parameters
     * @param Response $response
     * @return Response
     */
    public function render($view, array $parameters = array(), Response $response = null)
    {
        $parameters = array_merge($parameters, array('notices' => $this->notices));

        return parent::render($view, $parameters, $response);
    }
} 