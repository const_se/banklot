<?php

namespace BanklotBundle\Controller;

use BanklotBundle\Entity\Lot;
use BanklotBundle\Entity\LotInvite;
use BanklotBundle\Entity\Notice;
use BanklotBundle\Entity\Role;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Cfg;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends InitializableController
{
    /**
     * @param Request $request
     * @param $ids
     * @return JsonResponse
     * @Cfg\Route("/set-notices-viewed/{ids}", name = "api_set_notices_viewed", requirements = {"ids": "^(\d+,?)+$"})
     */
    public function setNoticesViewed(Request $request, $ids)
    {
        if (!$request->isXmlHttpRequest()) throw $this->createNotFoundException();

        $ids = explode(',', $ids);

        foreach ($ids as $id) {
            $notice = $this->getRepository('Notice')->find($id);

            if (!is_null($notice)) {
                $notice->setViewed(true);
                $this->manager->persist($notice);
            }
        }

        $this->manager->flush();

        return new JsonResponse('ok');
    }

    /**
     * @param LotInvite $invite
     * @return JsonResponse
     * @Cfg\Route("/confirm-lot-invite/{invite}", name = "api_confirm_lot_invite", requirements = {"invite": "\d+"})
     * @Cfg\ParamConverter("invite", options = {"mapping": {"invite": "id"}})
     */
    public function confirmLotInvite(LotInvite $invite)
    {
        $user = $invite->getUser();
        $lot = $invite->getLot();
        $invite->setConfirmed(true);
        $this->manager->persist($invite);

        $lot->addParticipant($user);
        $this->manager->persist($lot);
        $this->manager->flush();

        return $this->redirectToRoute('lots_lot', array('lot' => $lot->getId()));
    }

    /**
     * @param Request $request
     * @param Lot $lot
     * @return JsonResponse
     * @Cfg\Route("/send-lot-join/{lot}", name = "api_send_lot_join", requirements = {"lot": "\d+"})
     * @cfg\ParamConverter("lot", options = {"mapping": {"lot": "id"}})
     */
    public function sendLotJoin(Request $request, Lot $lot)
    {
        if (!$this->authChecker->isGranted(Role::AUTHENTICATED_FULLY)) throw $this->createNotFoundException();

        if (!$request->isXmlHttpRequest()) throw $this->createNotFoundException();

        $invite = new LotInvite();
        $invite->setLot($lot)
            ->setUser($this->getUser());
        $this->manager->persist($invite);
        $this->manager->flush();

        $notice = new Notice();
        $notice->setType(Notice::USER_JOIN)
            ->setToUser($lot->getOwner())
            ->setOptions(array(
                'user' => $this->getUser()->getNickname(),
                'lot_id' => $lot->getId(),
                'lot_name' => $lot->getName(),
                'invite_id' => $invite->getId()
            ));
        $this->manager->persist($notice);
        $this->manager->flush();

        return new JsonResponse('ok');
    }
} 