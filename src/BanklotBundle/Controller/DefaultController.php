<?php

namespace BanklotBundle\Controller;

use BanklotBundle\Entity\User;
use BanklotBundle\Form\Type\Security\Registration1Type;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Cfg;

class DefaultController extends InitializableController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/", name = "default_index")
     */
    public function indexAction()
    {
        $form = $this->createForm(new Registration1Type(), new User());

        return $this->render('BanklotBundle:Default:index.html.twig', array('form' => $form->createView()));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/invite", name = "default_invite")
     */
    public function inviteAction()
    {
        return $this->render('BanklotBundle:Default:invite.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/notices", name = "default_notices")
     */
    public function noticesAction()
    {
        $notices = $this->getRepository('Notice')->createQueryBuilder('n')
            ->where('n.toUser = :user')
            ->setParameter('user', $this->getUser())
            ->orderBy('n.createdAt', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('BanklotBundle:Default:notices.html.twig', array('all_notices' => $notices));
    }
} 