<?php

namespace BanklotBundle\Controller;

use BanklotBundle\Entity\Lot;
use BanklotBundle\Entity\LotComment;
use BanklotBundle\Entity\LotInvite;
use BanklotBundle\Entity\Money;
use BanklotBundle\Entity\Notice;
use BanklotBundle\Entity\Repository\ConfigRepository;
use BanklotBundle\Entity\Role;
use BanklotBundle\Entity\UploadedFile;
use BanklotBundle\Entity\User;
use BanklotBundle\Form\Type\Lots\AddLotType;
use BanklotBundle\Form\Type\Lots\LotCommentType;
use BanklotBundle\Form\Type\Lots\LotFundsType;
use BanklotBundle\Form\Type\Lots\LotsFilterType;
use BanklotBundle\Form\Type\Lots\LotStatusBoughtType;
use BanklotBundle\Form\Type\Lots\LotStatusCapitalizedType;
use BanklotBundle\Form\Type\Lots\LotStatusNotBoughtType;
use BanklotBundle\Form\Type\Lots\LotStatusPurchaseType;
use BanklotBundle\Form\Type\Lots\LotStatusRaisingType;
use BanklotBundle\Form\Type\Lots\LotStatusType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Cfg;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

class LotsController extends InitializableController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/add-lot", name = "lots_add_lot")
     */
    public function addLotAction(Request $request)
    {
        if (!$this->checkAccess()) return $this->redirectToRoute('security_access_denied');

        $lot = new Lot();
        $form = $this->createForm(new AddLotType(), $lot);
        $form->get('files')->setData(array(new UploadedFile()));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $valid = true;
            $files = $form->get('files');
            $hasFiles = false;

            foreach ($files as $fileForm) {
                if (!is_null($fileForm->getData())) {
                    /** @var UploadedFile $file */
                    $file = $fileForm->getData();
                    if ($file->getFile()) {
                        $hasFiles = true;
                        $file->upload('uploads/lots');
                        $this->manager->persist($file);
                    }
                }
            }

            if (!$hasFiles) $lot->getFiles()->clear();

            /** @var \BanklotBundle\Entity\User $user */
            $user = $this->getUser();
            $lot->setOwner($user)
                ->addParticipant($user);
            $this->manager->persist($lot);

            $participants = $form->get('participants')->getData();

            if ($lot->isPrivate() && !empty($participants)) {
                $participants = preg_replace('/[,; ]+/', '#', $participants);
                $participants = explode('#', $participants);

                foreach ($participants as $nickname) {
                    $puser = $this->getRepository('User')->createQueryBuilder('u')
                        ->where('u.nickname = :nickname')
                        ->setParameter('nickname', $nickname)
                        ->getQuery()
                        ->getOneOrNullResult();

                    if (is_null($puser)) {
                        $form->get('participants')->addError(new FormError('Пользователь ' . $nickname . ' не найден'));
                        $valid = false;
                        continue;
                    }

                    $invite = new LotInvite();
                    $invite->setLot($lot)
                        ->setUser($puser);
                    $this->manager->persist($invite);
                }
            }

            if ($valid) {
                $ownFunds = $form->get('ownFunds')->getData();
                /** @var \BanklotBundle\Entity\User $user */
                $user = $this->getUser();
                $purchase = $lot->getPurchaseCost() + $lot->getAdditionalExpenses();

                if (!is_numeric($ownFunds)) {
                    $form->get('ownFunds')->addError(new FormError('Некорректная сумма собственных инвестиций'));
                    $valid = false;
                }
                elseif ($ownFunds <= 0) {
                    $form->get('ownFunds')->addError(new FormError('Сумма собственных инвестиций не может быть отрицательной'));
                    $valid = false;
                }
                elseif ($ownFunds > $user->getBalance()) {
                    $form->get('ownFunds')->addError(new FormError('Недостаточно средств'));
                    $valid = false;
                }
                elseif ($ownFunds > $purchase) $ownFunds = $purchase;

                if (!$valid) return false;

                $money = new Money();
                $money->setConfirmed(true)
                    ->setLot($lot)
                    ->setSum($ownFunds)
                    ->setType(Money::TO_LOT)
                    ->setUser($user);
                $this->manager->persist($money);

                $lot->addMoney($money);
                $this->manager->persist($lot);

                $user->setBalance($user->getBalance() - $ownFunds);
                $this->manager->persist($user);

                if ($ownFunds === $purchase) {
                    $lot->setStatus(Lot::PURCHASE);
                    $this->manager->persist($lot);

                    foreach ($lot->getParticipants() as $puser) {
                        $notice = new Notice();
                        $notice->setLot($lot)
                            ->setToUser($puser)
                            ->setType(Notice::LOT_PURCHASE);
                        $this->manager->persist($notice);
                    }
                }
            }

            if ($valid) {
                $this->manager->flush();

                $invites = $this->getRepository('LotInvite')->createQueryBuilder('i')
                    ->where('i.lot = :lot')
                    ->setParameter('lot', $lot)
                    ->getQuery()
                    ->getResult();

                /** @var LotInvite $invite */
                foreach ($invites as $invite) {
                    $notice = new Notice();
                    $notice->setToUser($invite->getUser())
                        ->setType(Notice::USER_INVITE)
                        ->setOptions(array(
                            'user' => $invite->getUser()->getNickname(),
                            'lot_id' => $lot->getId(),
                            'lot_name' => $lot->getName(),
                            'invite_id' => $invite->getId()
                        ));
                    $this->manager->persist($notice);
                }

                $this->manager->flush();

                return $this->redirectToRoute('lots_own');
            }
        }

        return $this->render('BanklotBundle:Lots:add_lot.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param Request $request
     * @param string $filter
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/{filter}", name = "lots_all", defaults = {"filter": "all"}, requirements = {"filter": "all|opened|closed"})
     */
    public function allLotsAction(Request $request, $filter = 'all')
    {
        if (!$this->checkAccess()) return $this->redirectToRoute('security_access_denied');

        $lots = $this->getRepository('Lot')->createQueryBuilder('l')
            ->select('l, c')
            ->leftJoin('l.category', 'c')
            ->where('l.status = :status')
            ->setParameter('status', Lot::FUND_RAISING)
            ->orderBy('l.name', 'ASC');

        switch ($filter) {
            case 'opened': $lots->andWhere('l.private = :private')->setParameter('private', false); break;
            case 'closed': $lots->andWhere('l.private = :private')->setParameter('private', true); break;
        }

        $form = $this->createForm(new LotsFilterType());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category = $form->get('category')->getData();
            $search = $form->get('search')->getData();

            if (!empty($category)) $lots->andWhere('l.category = :category')->setParameter('category', $category);

            if (!empty($search)) $lots->leftJoin('l.owner', 'o')->andWhere('l.name LIKE :search OR o.nickname LIKE :search')
                ->setParameter('search', '%' . $search . '%');
        }

        $lots = $lots->getQuery()
            ->getResult();

        $lotsCount = $this->getRepository('Lot')->createQueryBuilder('l')
            ->select('COUNT(l.id)')
            ->where('l.status = :status')
            ->setParameter('status', Lot::FUND_RAISING)
            ->getQuery()
            ->getSingleScalarResult();

        return $this->render('BanklotBundle:Lots:all_lots.html.twig', array(
            'lots_count' => $lotsCount,
            'form' => $form->createView(),
            'filter_active' => $filter,
            'lots' => $lots
        ));
    }

    /**
     * @param Request $request
     * @param Lot $lot
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/lot/{lot}", name = "lots_lot", requirements = {"lot": "\d+"})
     * @Cfg\ParamConverter("lot", options = {"mapping": {"lot": "id"}})
     */
    public function lotAction(Request $request, Lot $lot)
    {
        if (!$this->checkAccess()) return $this->redirectToRoute('security_access_denied');

        /** @var \BanklotBundle\Entity\User $user */
        $user = $this->getUser();
        $lotPart = false;

        foreach ($lot->getParticipants() as $puser) {
            if ($puser->getId() === $user->getId()) {
                $lotPart = true;
                break;
            }
        }

        if ($lot->isPrivate() && !$lotPart) return $this->redirectToRoute('lots_all');

        $comment = new LotComment();
        $formComment = $this->createForm(new LotCommentType(), $comment);
        $formComment->handleRequest($request);

        if ($formComment->isSubmitted() && $formComment->isValid()) {
            $comment->setLot($lot)
                ->setUser($user);
            $this->manager->persist($comment);
            $this->manager->flush();

            return $this->redirectToRoute('lots_lot', array('lot' => $lot->getId()));
        }

        $ownFunds = 0;

        if ($lotPart)
            $ownFunds = $this->getRepository('Money')->createQueryBuilder('m')
                ->select('SUM(m.sum)')
                ->where('m.user = :user')
                ->andWhere('m.lot = :lot')
                ->andWhere('m.type = :type')
                ->setParameters(array('user' => $this->getUser(), 'lot' => $lot, 'type' => Money::TO_LOT))
                ->getQuery()
                ->getSingleScalarResult();

        $lotFunds = 0.00;
        $form = null;

        if ($lot->getStatus() == Lot::FUND_RAISING) {
            $lotFunds = $this->getRepository('Money')->createQueryBuilder('m')
                ->select('SUM(m.sum)')
                ->andWhere('m.lot = :lot')
                ->setParameters(array('lot' => $lot))
                ->getQuery()
                ->getSingleScalarResult();

            $money = new Money();
            $form = $this->createForm(new LotFundsType(), $money);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $valid = true;

                if ($money->getSum() > $user->getBalance()) {
                    $form->get('sum')->addError(new FormError('Недостаточно средств'));
                    $valid = false;
                }

                if ($valid) {
                    if (!$lot->hasParticipant($user)) {
                        $lot->addParticipant($user);
                        $this->manager->persist($lot);
                    }

                    $sum = $lot->getPurchaseCost() + $lot->getAdditionalExpenses() - $lotFunds;

                    if ($money->getSum() > $sum) $money->setSum($sum);

                    $lotSum = $lotFunds + $money->getSum();

                    $money->setConfirmed(true)
                        ->setLot($lot)
                        ->setSum($money->getSum())
                        ->setType(Money::TO_LOT)
                        ->setUser($user);
                    $this->manager->persist($money);
                    $lot->addMoney($money);

                    $user->setBalance($user->getBalance() - $money->getSum());
                    $this->manager->persist($user);

                    foreach ($lot->getParticipants() as $puser) {
                        $notice = new Notice();
                        $notice->setToUser($puser)
                            ->setType(Notice::LOT_RAISING)
                            ->setOptions(array(
                                'lot_id' => $lot->getId(),
                                'lot_name' => $lot->getName(),
                                'sum' => $lotSum
                            ));
                        $this->manager->persist($notice);
                    }

                    if ($money->getSum() === $sum) {
                        $lot->setStatus(Lot::PURCHASE);
                        $this->manager->persist($lot);

                        foreach ($lot->getParticipants() as $puser) {
                            $notice = new Notice();
                            $notice->setToUser($puser)
                                ->setType(Notice::LOT_PURCHASE)
                                ->setOptions(array(
                                    'lot_id' => $lot->getId(),
                                    'lot_name' => $lot->getName()
                                ));
                            $this->manager->persist($notice);
                        }
                    }

                    $this->manager->flush();

                    return $this->redirectToRoute('lots_lot', array('lot' => $lot->getId()));
                }
            }
        }

        /** @var ConfigRepository $config */
        $config = $this->getRepository('Config');
        $comis = $config->get('capitalized_percent', 50);

        if (!is_null($user->getCertificate())) $comis = $config->get('capitalized_certificate_percent', 30);

        return $this->render('BanklotBundle:Lots:lot.html.twig', array(
            'lot' => $lot,
            'lot_part' => $lotPart,
            'own_funds' => $ownFunds,
            'lot_funds' => $lotFunds,
            'form' => !is_null($form) ? $form->createView() : null,
            'comis' => $comis,
            'form_comment' => $formComment->createView()
        ));
    }

    /**
     * @param Request $request
     * @param Lot $lot
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/edit/{lot}", name = "lots_edit", requirements = {"lot": "\d+"})
     * @Cfg\ParamConverter("lot", options = {"mapping": {"lot": "id"}})
     */
    public function lotEditAction(Request $request, Lot $lot)
    {
        if (!$this->authChecker->isGranted(Role::ADMIN) && $lot->getOwner()->getId() != $this->getUser()->getId())
            return $this->redirectToRoute('lots_all');

        $form = $this->createForm(new LotStatusType($lot->getStatus()));
        $form0 = $this->createForm(new LotStatusRaisingType(), $lot);
        $form1 = $this->createForm(new LotStatusPurchaseType(), $lot);
        $form2 = $this->createForm(new LotStatusBoughtType(), $lot);
        $form3 = $this->createForm(new LotStatusNotBoughtType(), $lot);
        $form4 = $this->createForm(new LotStatusCapitalizedType(), $lot);

        $form0->get('files')->setData(array(new UploadedFile()));
        $form0->handleRequest($request);

        if ($form0->isSubmitted() && $form0->isValid()) {
            $files = $form0->get('files')->getData();

            /** @var UploadedFile $file */
            foreach ($files as $file) {
                if (!is_null($file->getFile())) {
                    $file->upload('uploads/lots');
                    $this->manager->persist($file);
                    $lot->addFile($file);
                }
            }

            $this->manager->persist($lot);
            $this->manager->flush();

            return $this->redirectToRoute('lots_lot', array('lot' => $lot->getId()));
        }

        $form1->get('files')->setData(array(new UploadedFile()));
        $form1->handleRequest($request);

        if ($form1->isSubmitted() && $form1->isValid()) {
            $files = $form1->get('files')->getData();

            /** @var UploadedFile $file */
            foreach ($files as $file) {
                if (!is_null($file->getFile())) {
                    $file->upload('uploads/lots');
                    $this->manager->persist($file);
                    $lot->addFile($file);
                }
            }

            $this->manager->persist($lot);
            $this->manager->flush();

            return $this->redirectToRoute('lots_lot', array('lot' => $lot->getId()));
        }

        $form2->get('files')->setData(array(new UploadedFile()));
        $form2->handleRequest($request);

        if ($form2->isSubmitted() && $form2->isValid()) {
            $files = $form2->get('files')->getData();

            /** @var UploadedFile $file */
            foreach ($files as $file) {
                if (!is_null($file->getFile())) {
                    $file->upload('uploads/lots');
                    $this->manager->persist($file);
                    $lot->addFile($file);
                }
            }

            $this->manager->persist($lot);

            foreach ($lot->getParticipants() as $puser) {
                $notice = new Notice();
                $notice->setToUser($puser)
                    ->setType(Notice::LOT_BOUGHT)
                    ->setOptions(array(
                        'lot_id' => $lot->getId(),
                        'lot_name' => $lot->getName()
                    ));
                $this->manager->persist($notice);
            }

            $this->manager->flush();

            return $this->redirectToRoute('lots_lot', array('lot' => $lot->getId()));
        }

        $form3->get('files')->setData(array(new UploadedFile()));
        $form3->handleRequest($request);

        if ($form3->isSubmitted() && $form3->isValid()) {
            $files = $form3->get('files')->getData();

            /** @var UploadedFile $file */
            foreach ($files as $file) {
                if (!is_null($file->getFile())) {
                    $file->upload('uploads/lots');
                    $this->manager->persist($file);
                    $lot->addFile($file);
                }
            }

            $this->manager->persist($lot);

            foreach ($lot->getParticipants() as $puser) {
                $notice = new Notice();
                $notice->setToUser($puser)
                    ->setType(Notice::LOT_NOT_BOUGHT)
                    ->setOptions(array(
                        'lot_id' => $lot->getId(),
                        'lot_name' => $lot->getName()
                    ));
                $this->manager->persist($notice);

                $own = $this->getRepository('Money')->createQueryBuilder('m')
                    ->select('SUM(m.sum)')
                    ->where('m.lot = :lot')
                    ->andWhere('m.user = :user')
                    ->andWhere('m.type = :type')
                    ->setParameters(array('lot' => $lot, 'user' => $puser, 'type' => Money::TO_LOT))
                    ->getQuery()
                    ->getSingleScalarResult();
                $percentage = $lot->getPurchaseCost() != 0 ? $own / $lot->getPurchaseCost() * 100.00 : 0;
                $back = $lot->getPurchaseCost() != 0 ? ($lot->getPurchaseCost() - $lot->getAdditionalExpenses()) * $percentage / 100.00 : 0;

                $money = new Money();
                $money->setConfirmed(true)
                    ->setLot($lot)
                    ->setSum($back)
                    ->setType(Money::FROM_LOT_NOT_BOUGHT)
                    ->setUser($puser);
                $this->manager->persist($money);
                $puser->setBalance($puser->getBalance() + $back);
                $this->manager->persist($puser);
            }

            $this->manager->flush();

            return $this->redirectToRoute('lots_lot', array('lot' => $lot->getId()));
        }

        $form4->get('files')->setData(array(new UploadedFile()));
        $form4->handleRequest($request);

        if ($form4->isSubmitted() && $form4->isValid()) {
            $files = $form4->get('files')->getData();

            /** @var UploadedFile $file */
            foreach ($files as $file) {
                if (!is_null($file->getFile())) {
                    $file->upload('uploads/lots');
                    $this->manager->persist($file);
                    $lot->addFile($file);
                }
            }

            $this->manager->persist($lot);

            /** @var ConfigRepository $config */
            $config = $this->getRepository('Config');
            $profitAll = $lot->getSale() - $lot->getPurchaseCost() - $lot->getAdditionalExpenses();
            $own_percent = $config->get('own_percent', 1);
            if (!is_null($lot->getOwnerPercent())) $own_percent = $lot->getOwnerPercent();
            $bonus = $profitAll * $own_percent / 100.00;
            $owner = $lot->getOwner();
            $mbonus = new Money();
            $mbonus->setConfirmed(true)
                ->setUser($owner)
                ->setLot($lot)
                ->setType(Money::OWNER_BONUS)
                ->setSum($bonus);
            $this->manager->persist($mbonus);
            $owner->setBalance($owner->getBalance() + $bonus);
            $this->manager->persist($owner);

            if (!is_null($lot->getResponsible())) {
                $responsible = $lot->getResponsible();
                $responsible_percent = $config->get('responsible_percent', 10);
                if (!is_null($lot->getResponsiblePercent())) $responsible_percent = $lot->getResponsiblePercent();
                $rbonus = $profitAll * $responsible_percent / 100.00;
                $rmbonus = new Money();
                $rmbonus->setConfirmed(true)
                    ->setUser($responsible)
                    ->setLot($lot)
                    ->setType(Money::RESPONSIBLE_BONUS)
                    ->setSum($rbonus);
                $this->manager->persist($rmbonus);
                $responsible->setBalance($responsible->getBalance() + $rbonus);
                $this->manager->persist($responsible);
                $profitAll -= $rbonus;
            }

            $profitAll -= $bonus;

            foreach ($lot->getParticipants() as $puser) {
                $notice = new Notice();
                $notice->setToUser($puser)
                    ->setType(Notice::LOT_CAPITALIZED)
                    ->setOptions(array(
                        'lot_id' => $lot->getId(),
                        'lot_name' => $lot->getName()
                    ));
                $this->manager->persist($notice);

                $own = $this->getRepository('Money')->createQueryBuilder('m')
                    ->select('SUM(m.sum)')
                    ->where('m.lot = :lot')
                    ->andWhere('m.user = :user')
                    ->andWhere('m.type = :type')
                    ->setParameters(array('lot' => $lot, 'user' => $puser, 'type' => Money::TO_LOT))
                    ->getQuery()
                    ->getSingleScalarResult();

                $percentage = $lot->getPurchaseCost() != 0 ? $own / $lot->getPurchaseCost() * 100.00 : 0;
                $profit = $profitAll * $percentage / 100.00;
                $cert = !is_null($puser->getCertificate());
                $capPercent = !is_null($lot->getCapitalizedPercent());
                $capCertPercent = !is_null($lot->getCapitalizedCertificatePercent());

                if ($cert) {
                    if ($capCertPercent) $comis = $lot->getCapitalizedCertificatePercent();
                    else $comis = $config->get('capitalized_certificate_percent', 30);
                }
                else {
                    if ($capPercent) $comis = $lot->getCapitalizedPercent();
                    else $comis = $config->get('capitalized_percent', 50);
                }

                $back = $own + $profit * (100 - $comis) / 100.00;
                $money = new Money();
                $money->setConfirmed(true)
                    ->setLot($lot)
                    ->setSum($back)
                    ->setType(Money::FROM_LOT_CAPITALIZE)
                    ->setUser($puser);
                $this->manager->persist($money);
                $puser->setBalance($puser->getBalance() + $back);
                $this->manager->persist($puser);

                $p = $config->get('profit_capitalized', 0);
                $this->manager->persist($config->set('profit_capitalized', $p + $profit * $comis / 100.00));
            }

            $this->manager->flush();

            return $this->redirectToRoute('lots_lot', array('lot' => $lot->getId()));
        }

        return $this->render('BanklotBundle:Lots:edit_lot.html.twig', array(
            'lot' => $lot,
            'form' => $form->createView(),
            'form0' => $form0->createView(),
            'form1' => $form1->createView(),
            'form2' => $form2->createView(),
            'form3' => $form3->createView(),
            'form4' => $form4->createView()
        ));
    }

    /**
     * @param $filter
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/own/{filter}", name = "lots_own", defaults = {"filter": "0"}, requirements = {"filter": "0|1|2|3|4"})
     */
    public function ownLotsAction($filter = 0)
    {
        if (!$this->checkAccess()) return $this->redirectToRoute('security_access_denied');

        $lots = $this->getRepository('Lot')->createQueryBuilder('l')
            ->select('l, c')
            ->leftJoin('l.category', 'c')
            ->where('l.owner = :owner')
            ->setParameter('owner', $this->getUser())
            ->orderBy('l.name', 'ASC');

        if ($filter != 0) $lots->andWhere('l.status = :status')->setParameter('status', $filter);

        $lots = $lots->getQuery()
            ->getResult();

        $lotsCount = $this->getRepository('Lot')->createQueryBuilder('l')
            ->select('COUNT(l.id)')
            ->where('l.owner = :owner')
            ->setParameter('owner', $this->getUser())
            ->getQuery()
            ->getSingleScalarResult();

        return $this->render('BanklotBundle:Lots:own_lots.html.twig', array(
            'lots' => $lots,
            'lots_count' => $lotsCount,
            'filter_active' => $filter
        ));
    }

    /**
     * @param $filter
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/part/{filter}", name = "lots_part", defaults = {"filter": "0"}, requirements = {"filter": "0|1|2|3|4"})
     */
    public function partLotsAction($filter = 0)
    {
        if (!$this->checkAccess()) return $this->redirectToRoute('security_access_denied');

        $lots = $this->getRepository('Lot')->createQueryBuilder('l')
            ->select('l, c')
            ->leftJoin('l.category', 'c')
            ->leftJoin('l.participants', 'p')
            ->where('p = :owner OR l.owner = :owner')
            ->setParameter('owner', $this->getUser())
            ->orderBy('l.name', 'ASC');

        if ($filter != 0) $lots->andWhere('l.status = :status')->setParameter('status', $filter);

        $lots = $lots->getQuery()
            ->getResult();

        $lotsCount = $this->getRepository('Lot')->createQueryBuilder('l')
            ->select('COUNT(l.id)')
            ->leftJoin('l.participants', 'p')
            ->where('p = :user')
            ->andWhere('l.status <> :statusNotBought')
            ->andWhere('l.status <> :statusCapitalized')
            ->setParameters(array('user' => $this->getUser(), 'statusNotBought' => Lot::NOT_BOUGHT, 'statusCapitalized' => Lot::CAPITALIZED))
            ->getQuery()
            ->getSingleScalarResult();

        return $this->render('BanklotBundle:Lots:part_lots.html.twig', array(
            'lots' => $lots,
            'lots_count' => $lotsCount,
            'filter_active' => $filter
        ));
    }

    protected function addLotHandleMoney(Lot $lot, Form $form)
    {
        $valid = true;


        return true;
    }

    protected function addLotHandleParticipants(Lot $lot, Form $form)
    {

    }
} 