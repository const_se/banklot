<?php

namespace BanklotBundle\Controller;

use BanklotBundle\Entity\Lot;
use BanklotBundle\Entity\Money;
use BanklotBundle\Entity\Repository\ConfigRepository;
use BanklotBundle\Entity\User;
use BanklotBundle\Form\Type\Balance\RechargeType;
use BanklotBundle\Form\Type\Balance\WithdrawType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Cfg;

class BalanceController extends InitializableController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/recharge", name = "balance_recharge")
     */
    public function rechargeAction(Request $request)
    {
        $money = new Money();
        $form = $this->createForm(new RechargeType(), $money);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $money->setUser($this->getUser())
                ->setType(Money::RECHARGE);

            $this->manager->persist($money);
            $this->manager->flush();

            return $this->redirectToRoute('balance_recharge_confirm', array('money' => $money->getId()));
        }

        return $this->render('BanklotBundle:Balance:recharge.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param Money $money
     * @param $print
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/recharge-confirm/{money}/{print}", name = "balance_recharge_confirm", defaults = {"print": 0}, requirements = {"money": "\d+", "print": "0|1"})
     * @Cfg\ParamConverter("money", options = {"mapping": {"money": "id"}})
     */
    public function rechargeConfirmAction(Money $money, $print = false)
    {
        if ($print) return $this->render('BanklotBundle:Balance:recharge_confirm_print.html.twig', array('money' => $money));

        return $this->render('BanklotBundle:Balance:recharge_confirm.html.twig', array('money' => $money));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/status", name = "balance_status")
     */
    public function statusAction()
    {
        $user = $this->getUser();
        $date = new \DateTime();
        $date->sub(new \DateInterval('P3M'));

        $moneys = $this->getRepository('Money')->createQueryBuilder('m')
            ->where('m.user = :user')
            ->andWhere('m.createdAt >= :date')
            ->setParameters(array('user' => $user, 'date' => $date))
            ->orderBy('m.createdAt', 'DESC')
            ->getQuery()
            ->getResult();

        $moneyNotConfirmed = $this->getRepository('Money')->createQueryBuilder('m')
            ->select('SUM(m.sum)')
            ->leftJoin('m.lot', 'l')
            ->where('m.user = :user')
            ->andWhere('m.confirmed = :confirmed')
            ->andWhere('l.status <> :status')
            ->setParameters(array('user' => $user, 'confirmed' => false, 'status' => Lot::CAPITALIZED))
            ->getQuery()
            ->getSingleScalarResult();

        $moneyLots = $this->getRepository('Money')->createQueryBuilder('m')
            ->select('SUM(m.sum)')
            ->where('m.user = :user')
            ->andWhere('m.lot IS NOT NULL')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();

        $moneyAll = $this->getRepository('User')->createQueryBuilder('u')
            ->select('SUM(u.balance)')
            ->getQuery()
            ->getSingleScalarResult();

        return $this->render('BanklotBundle:Balance:status.html.twig', array(
            'moneys' => $moneys,
            'money_not_confirmed' => $moneyNotConfirmed,
            'money_lots' => $moneyLots,
            'money_all' => $moneyAll
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/withdraw", name = "balance_withdraw")
     */
    public function withdrawAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        $money = new Money();
        $form = $this->createForm(new WithdrawType(), $money);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $valid = true;

            $money->setUser($user)
                ->setType(Money::WITHDRAW);
            $this->manager->persist($money);

            if ($money->getSum() > $user->getBalance()) {
                $form->get('sum')->addError(new FormError('Недостаточно средств'));
                $valid = false;
            }

            if ($valid) {
                $user->setBalance($user->getBalance() - $money->getSum());
                $this->manager->persist($user);
                $this->manager->flush();

                return $this->redirectToRoute('balance_withdraw_confirm', array('money' => $money->getId()));
            }
        }

        /** @var ConfigRepository $config */
        $config = $this->getRepository('Config');
        $percent = $config->get('withdraw_percent', 8);

        if (!is_null($user->getCertificate())) $percent = $config->get('withdraw_certificate_percent', 5);

        return $this->render('BanklotBundle:Balance:withdraw.html.twig', array('form' => $form->createView(), 'percent' => $percent));
    }

    /**
     * @param Money $money
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/withdraw-confirm/{money}", name = "balance_withdraw_confirm", requirements = {"money": "\d+"})
     * @Cfg\ParamConverter("money", options = {"mapping": {"money": "id"}})
     */
    public function withdrawConfirmAction(Money $money)
    {
        /** @var ConfigRepository $config */
        $config = $this->getRepository('Config');
        $percent = $config->get('withdraw_percent', 8);

        if (!is_null($this->getUser()->getCertificate())) $percent = $config->get('withdraw_certificate_percent', 5);

        return $this->render('BanklotBundle:Balance:withdraw_confirm.html.twig', array('money' => $money, 'percent' => $percent));
    }
} 