<?php

namespace BanklotBundle\Controller;

use BanklotBundle\Entity\Certificate;
use BanklotBundle\Entity\LotCategory;
use BanklotBundle\Entity\Money;
use BanklotBundle\Entity\Repository\ConfigRepository;
use BanklotBundle\Entity\User;
use BanklotBundle\Form\Type\Admin\CertificateType;
use BanklotBundle\Form\Type\Admin\ConfigType;
use BanklotBundle\Form\Type\Admin\LotCategoryType;
use BanklotBundle\Form\Type\Admin\MoneyType;
use BanklotBundle\Form\Type\Admin\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Cfg;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends InitializableController
{
    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Cfg\Route("/", name = "admin_index")
     */
    public function adminAction()
    {
        return $this->redirectToRoute('admin_moneys');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/lot-categories", name = "admin_lot_categories")
     */
    public function lotCategoriesAction()
    {
        $categories = $this->getRepository('LotCategory')->createQueryBuilder('c')
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->render('BanklotBundle:Admin:lot_categories.html.twig', array('categories' => $categories));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/lot-categories-add", name = "admin_lot_categories_add")
     */
    public function lotCategoryAddAction(Request $request)
    {
        $category = new LotCategory();
        $form = $this->createForm(new LotCategoryType(), $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($category);
            $this->manager->flush();

            return $this->redirectToRoute('admin_lot_categories');
        }

        return $this->render('BanklotBundle:Admin:lot_categories_add.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param Request $request
     * @param LotCategory $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/lot-categories-edit/{category}", name = "admin_lot_categories_edit", requirements = {"category": "\d+"})
     * @Cfg\ParamConverter("category", options = {"mapping": {"category": "id"}})
     */
    public function lotCategoryEdit(Request $request, LotCategory $category)
    {
        $form = $this->createForm(new LotCategoryType(), $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($category);
            $this->manager->flush();

            return $this->redirectToRoute('admin_lot_categories');
        }

        return $this->render('BanklotBundle:Admin:lot_categories_edit.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param LotCategory $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Cfg\Route("/lot-categories-remove/{category}", name = "admin_lot_categories_remove", requirements = {"category": "\d+"})
     * @Cfg\ParamConverter("category", options = {"mapping": {"category": "id"}})
     */
    public function lotCategoryRemove(LotCategory $category)
    {
        $this->manager->remove($category);
        $this->manager->flush();

        return $this->redirectToRoute('admin_lot_categories');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/moneys", name = "admin_moneys")
     */
    public function moneysAction()
    {
        $moneys = $this->getRepository('Money')->createQueryBuilder('m')
            ->where('m.confirmed = :confirmed')
            ->setParameter('confirmed', false)
            ->orderBy('m.createdAt', 'DESC')
            ->getQuery()
            ->getResult();

        /** @var ConfigRepository $config */
        $config = $this->getRepository('Config');
        $percent = $config->get('withdraw_percent', 8);
        $certPercent = $config->get('withdraw_certificate_percent', 5);

        $allMoneys = $this->getRepository('User')->createQueryBuilder('u')
            ->select('SUM(u.balance)')
            ->getQuery()
            ->getSingleScalarResult();

        $moneysAll = $this->getRepository('Money')->createQueryBuilder('m')
            ->orderBy('m.createdAt', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('BanklotBundle:Admin:moneys.html.twig', array(
            'moneys' => $moneys,
            'percent' => $percent,
            'cert_percent' => $certPercent,
            'all_moneys' => $allMoneys,
            'profit_withdraw' => $config->get('profit_withdraw', 0),
            'profit_capitalized' => $config->get('profit_capitalized', 0),
            'moneys_all' => $moneysAll
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/moneys-add", name = "admin_moneys_add")
     */
    public function moneysAddAction(Request $request)
    {
        $money = new Money();
        $form = $this->createForm(new MoneyType(), $money);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($money);
            $this->manager->flush();

            return $this->redirectToRoute('admin_moneys');
        }

        return $this->render('BanklotBundle:Admin:moneys_add.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param Money $money
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Cfg\Route("/moneys-confirm/{money}", name = "admin_moneys_confirm", requirements = {"money": "\d+"})
     * @Cfg\ParamConverter("money", options = {"mapping": {"money": "id"}})
     */
    public function moneysConfirmAction(Money $money)
    {
        $money->setConfirmed(true);
        $user = $money->getUser();
        /** @var ConfigRepository $config */
        $config = $this->getRepository('Config');

        if ($money->getType() == Money::RECHARGE) {
            $user->setBalance($user->getBalance() + $money->getSum());
            $this->manager->persist($user);
        }
        elseif ($money->getType() == Money::WITHDRAW) {
            $profit = $config->get('profit_withdraw', 0);
            $comis = $config->get('withdraw_percent', 8);

            if (!is_null($user->getCertificate())) $comis = $config->get('withdraw_certificate_percent', 5);
            $this->manager->persist($config->set('profit_withdraw', $profit + $money->getSum() * $comis / 100.00));
        }

        $this->manager->persist($money);
        $this->manager->flush();

        return $this->redirectToRoute('admin_moneys');
    }

    /**
     * @param Request $request
     * @param Money $money
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/moneys-edit/{money}", name = "admin_moneys_edit", requirements = {"money": "\d+"})
     * @Cfg\ParamConverter("money", options = {"mapping": {"money": "id"}})
     */
    public function moneysEditAction(Request $request, Money $money)
    {
        $form = $this->createForm(new MoneyType(), $money);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($money);
            $this->manager->flush();

            return $this->redirectToRoute('admin_moneys');
        }

        return $this->render('BanklotBundle:Admin:moneys_edit.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param Money $money
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Cfg\Route("/moneys-remove/{money}", name = "admin_moneys_remove", requirements = {"money": "\d+"})
     * @Cfg\ParamConverter("money", options = {"mapping": {"money": "id"}})
     */
    public function moneysRemoveAction(Money $money)
    {
        $this->manager->remove($money);
        $this->manager->flush();

        return $this->redirectToRoute('admin_moneys');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/users", name = "admin_users")
     */
    public function usersAction()
    {
        $users = $this->getRepository('User')->createQueryBuilder('u')
            ->orderBy('u.name', 'ASC')
            ->where('u.status = :status')
            ->setParameter('status', User::REGISTERED)
            ->getQuery()
            ->getResult();

        return $this->render('BanklotBundle:Admin:users.html.twig', array('users' => $users));
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/users-edit/{user}", name = "admin_users_edit", requirements = {"user": "\d+"})
     * @Cfg\ParamConverter("user", options = {"mapping": {"user": "id"}})
     */
    public function usersEditAction(Request $request, User $user)
    {
        $form = $this->createForm(new UserType(), $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($user);
            $this->manager->flush();

            return $this->redirectToRoute('admin_users');
        }

        return $this->render('BanklotBundle:Admin:users_edit.html.twig', array('form' => $form->createView(), 'user' => $user));
    }

    /**
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Cfg\Route("/users-lock/{user}", name = "admin_users_lock", requirements = {"user": "\d+"})
     * @Cfg\ParamConverter("user", options = {"mapping": {"user": "id"}})
     */
    public function usersLockAction(User $user)
    {
        $user->setLocked(!$user->isLocked());
        $this->manager->persist($user);
        $this->manager->flush();

        return $this->redirectToRoute('admin_users');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/users-invites", name = "admin_users_invites")
     */
    public function usersInvites()
    {
        $users = $this->getRepository('User')->createQueryBuilder('u')
            ->select('u, i')
            ->leftJoin('u.invited', 'i', 'WITH', 'i.status = :status')
            ->where('u.status = :status')
            ->setParameter('status', User::REGISTERED)
            ->orderBy('u.nickname', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->render('BanklotBundle:Admin:users_invites.html.twig', array('users' => $users));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/notices", name = "admin_notices")
     */
    public function noticesAction()
    {
        return $this->render('BanklotBundle:Admin:notices.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/lots", name = "admin_lots")
     */
    public function lotsAction()
    {
        $lots = $this->getRepository('Lot')->createQueryBuilder('l')
            ->orderBy('l.createdAt', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('BanklotBundle:Admin:lots.html.twig', array('lots' => $lots));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/config", name = "admin_config")
     */
    public function configAction(Request $request)
    {
        /** @var ConfigRepository $config */
        $config = $this->getRepository('Config');

        $form = $this->createForm(new ConfigType());
        $form->get('allowed_balance')->setData($config->get('allowed_balance', 0));
        $form->get('allowed_certificate_balance')->setData($config->get('allowed_certificate_balance', 0));
        $form->get('withdraw_percent')->setData($config->get('withdraw_percent', 0));
        $form->get('withdraw_certificate_percent')->setData($config->get('withdraw_certificate_percent', 0));
        $form->get('capitalized_percent')->setData($config->get('capitalized_percent', 0));
        $form->get('capitalized_certificate_percent')->setData($config->get('capitalized_certificate_percent', 0));
        $form->get('own_percent')->setData($config->get('own_percent', 1));
        $form->get('responsible_percent')->setData($config->get('responsible_percent', 10));
        $form->get('invite_percent')->setData($config->get('invite_percent', 0));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($config->set('allowed_balance', $form->get('allowed_balance')->getData()));
            $this->manager->persist($config->set('allowed_certificate_balance', $form->get('allowed_certificate_balance')->getData()));
            $this->manager->persist($config->set('withdraw_percent', $form->get('withdraw_percent')->getData()));
            $this->manager->persist($config->set('withdraw_certificate_percent', $form->get('withdraw_certificate_percent')->getData()));
            $this->manager->persist($config->set('capitalized_percent', $form->get('capitalized_percent')->getData()));
            $this->manager->persist($config->set('capitalized_certificate_percent', $form->get('capitalized_certificate_percent')->getData()));
            $this->manager->persist($config->set('own_percent', $form->get('own_percent')->getData()));
            $this->manager->persist($config->set('responsible_percent', $form->get('responsible_percent')->getData()));
            $this->manager->persist($config->set('invite_percent', $form->get('invite_percent')->getData()));

            $this->manager->flush();

            return $this->redirectToRoute('admin_config');
        }

        return $this->render('BanklotBundle:Admin:config.html.twig', array('form' => $form->createView()));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/certificates", name = "admin_certificates")
     */
    public function certificatesAction()
    {
        $certificates = $this->getRepository('Certificate')->createQueryBuilder('c')
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->render('BanklotBundle:Admin:certificates.html.twig', array('certificates' => $certificates));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Cfg\Route("/certificates-add", name = "admin_certificates_add")
     */
    public function certificatesAddAction(Request $request)
    {
        $certificate = new Certificate();
        $form = $this->createForm(new CertificateType(), $certificate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($certificate);
            $this->manager->flush();

            return $this->redirectToRoute('admin_certificates');
        }

        return $this->render('BanklotBundle:Admin:certificates_add.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param Certificate $certificate
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Cfg\Route("/certificates-remove/{certificate}", name = "admin_certificates_remove", requirements = {"certificate": "\d+"})
     * @Cfg\ParamConverter("certificate", options = {"mapping": {"certificate": "id"}})
     */
    public function certificatesRemoveAction(Certificate $certificate)
    {
        $this->manager->remove($certificate);
        $this->manager->flush();

        return $this->redirectToRoute('admin_certificates');
    }
}