<?php

namespace BanklotBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolver;

class AbstractEntityType extends AbstractType
{
    protected $dataClass;
    protected $validationGroups;

    public function __construct($name, $entity, $validationGroups = array(), $method = self::METHOD_POST)
    {
        parent::__construct($name, $method);
        $this->dataClass = 'BanklotBundle\Entity\\' . $entity;
        $this->validationGroups = is_array($validationGroups) ? $validationGroups : array($validationGroups);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->dataClass,
            'method' => $this->method,
        ));
    }
} 