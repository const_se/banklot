<?php

namespace BanklotBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolver;

class AbstractType extends \Symfony\Component\Form\AbstractType
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';

    protected $method;
    protected $name;

    public function __construct($name, $method = self::METHOD_POST)
    {
        $this->name = $name;
        $this->method = $method;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('method', $this->method);
    }

    public function getName()
    {
        return $this->name;
    }
} 