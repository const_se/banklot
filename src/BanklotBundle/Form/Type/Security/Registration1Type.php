<?php

namespace BanklotBundle\Form\Type\Security;

use BanklotBundle\Form\Type\AbstractEntityType;
use Symfony\Component\Form\FormBuilderInterface;

class Registration1Type extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('registration1', 'User', 'registration1');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', 'email', array('label' => 'Электропочта'))
            ->add('password', 'password', array('label' => 'Пароль'));
    }
} 