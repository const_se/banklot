<?php

namespace BanklotBundle\Form\Type\Security;

use BanklotBundle\Form\Type\AbstractEntityType;
use BanklotBundle\Form\Type\Common\UploadedFileType;
use Symfony\Component\Form\FormBuilderInterface;

class Registration3Type extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('registration3', 'User', 'registration3');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $today = new \DateTime();
        $builder->add('passportSeries', 'text', array('label' => false))
            ->add('passportNumber', 'text', array('label' => false))
            ->add('passportDateOfIssue', 'date', array('label' => 'Дата выдачи', 'years' => range(1920, $today->format('Y'))))
            ->add('passportIssuedBy', 'textarea', array('label' => 'Кем выдан'))
            ->add('passportPhoto', new UploadedFileType(), array('label' => 'Фото/скан паспорта', 'mapped' => false))
            ->add('INN', 'text', array('label' => 'Номер ИНН'))
            ->add('INNPhoto', new UploadedFileType(), array('label' => 'Фото/скан свидетельства', 'mapped' => false))
            ->add('bankAccount', 'text', array('label' => 'Номер счета'))
            ->add('bankBIK', 'text', array('label' => 'Код БИК'))
            ->add('certificate', 'text', array('label' => 'Номер сертификата', 'required' => false));
    }
} 