<?php

namespace BanklotBundle\Form\Type\Security;

use BanklotBundle\Form\Type\AbstractEntityType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminLoginType extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('login', 'AdminUser', 'login');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', 'text', array('label' => 'Логин'))
            ->add('password', 'password', array('label' => 'Пароль'))
            ->add('remember_me', 'hidden', array('data' => true, 'mapped' => false));
    }
}