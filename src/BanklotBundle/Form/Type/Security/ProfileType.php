<?php

namespace BanklotBundle\Form\Type\Security;

use BanklotBundle\Form\Type\AbstractEntityType;
use BanklotBundle\Form\Type\Common\UploadedFileType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileType extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('profile', 'User', 'profile');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $today = new \DateTime();
        $date = clone $today;
        $date->sub(new \DateInterval('P18Y'));
        $builder->add('name', 'text', array('label' => 'Фамилия, имя, отчество'))
            ->add('phone', 'text', array('label' => 'Телефон'))
            ->add('dateOfBirth', 'date', array('label' => 'Дата рождения', 'years' => range(1920, $date->format('Y'))))
            ->add('country', 'text', array('label' => 'Страна'))
            ->add('region', 'text', array('label' => 'Регион'))
            ->add('city', 'text', array('label' => 'Город'))
            ->add('address', 'textarea', array('label' => 'Почтовый адрес'))
            ->add('nickname', 'text', array('label' => 'Никнейм'))
            ->add('passportSeries', 'text', array('label' => false))
            ->add('passportNumber', 'text', array('label' => false))
            ->add('passportDateOfIssue', 'date', array('label' => 'Дата выдачи', 'years' => range(1920, $today->format('Y'))))
            ->add('passportIssuedBy', 'textarea', array('label' => 'Кем выдан'))
            ->add('passportPhoto', new UploadedFileType(), array('label' => 'Фото/скан паспорта', 'mapped' => false, 'required' => false))
            ->add('INN', 'text', array('label' => 'Номер ИНН'))
            ->add('INNPhoto', new UploadedFileType(), array('label' => 'Фото/скан свидетельства', 'mapped' => false, 'required' => false))
            ->add('bankAccount', 'text', array('label' => 'Номер счета'))
            ->add('bankBIK', 'text', array('label' => 'Код БИК'));
    }
} 