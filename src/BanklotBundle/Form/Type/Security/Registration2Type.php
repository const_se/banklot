<?php

namespace BanklotBundle\Form\Type\Security;

use BanklotBundle\Form\Type\AbstractEntityType;
use Symfony\Component\Form\FormBuilderInterface;

class Registration2Type extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('registration2', 'User', 'registration2');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $today = new \DateTime();
        $today->sub(new \DateInterval('P18Y'));
        $builder->add('name', 'text', array('label' => 'Фамилия, имя, отчество'))
            ->add('phone', 'text', array('label' => 'Телефон'))
            ->add('dateOfBirth', 'date', array('label' => 'Дата рождения', 'years' => range(1920, $today->format('Y'))))
            ->add('country', 'text', array('label' => 'Страна'))
            ->add('region', 'text', array('label' => 'Регион'))
            ->add('city', 'text', array('label' => 'Город'))
            ->add('address', 'textarea', array('label' => 'Почтовый адрес'))
            ->add('nickname', 'text', array('label' => 'Никнейм'));
    }
} 