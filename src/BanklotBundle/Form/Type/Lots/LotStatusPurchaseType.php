<?php

namespace BanklotBundle\Form\Type\Lots;

use BanklotBundle\Form\Type\AbstractEntityType;
use BanklotBundle\Form\Type\Common\UploadedFileType;
use Symfony\Component\Form\FormBuilderInterface;

class LotStatusPurchaseType extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('lot_status_purchase', 'Lot');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('description', 'textarea', array('label' => 'Описание'))
            ->add('status', 'hidden', array('data' => 1))
            ->add('files', 'collection', array('label' => 'Документы', 'mapped' => false,
                'type' => new UploadedFileType(),
                'allow_add' => true,
                'delete_empty' => true,
                'options' => array('label' => false, 'required' => false)
            ));
    }
} 