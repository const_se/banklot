<?php

namespace BanklotBundle\Form\Type\Lots;

use BanklotBundle\Form\Type\AbstractEntityType;
use BanklotBundle\Form\Type\Common\UploadedFileType;
use Symfony\Component\Form\FormBuilderInterface;

class LotStatusCapitalizedType extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('lot_status_capitalized', 'Lot');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('sale', 'number', array('label' => 'Стоимость продажи', 'scale' => 2))
            ->add('additionalExpenses', 'number', array('label' => 'Дополнительные затраты', 'scale' => 2))
            ->add('capitalization', 'integer', array('label' => 'Срок капитализации'))
            ->add('description', 'textarea', array('label' => 'Описание'))
            ->add('status', 'hidden', array('data' => 4))
            ->add('files', 'collection', array('label' => 'Документы', 'mapped' => false,
                'type' => new UploadedFileType(),
                'allow_add' => true,
                'delete_empty' => true,
                'options' => array('label' => false, 'required' => false)
            ))->add('capitalizedPercent', 'number', array('label' => 'Процент комиссии с чистой прибыли', 'scale' => 0, 'required' => false))
            ->add('capitalizedCertificatePercent', 'number', array('label' => 'Процент комиссии с чистой прибыли для учеников', 'scale' => 0, 'required' => false))
            ->add('ownerPercent', 'number', array('label' => 'Процент авторского вознаграждения', 'scale' => 0, 'required' => false))
            ->add('responsiblePercent', 'number', array('label' => 'Процент вознаграждения исполнителю', 'scale' => 0, 'required' => false));
    }
} 