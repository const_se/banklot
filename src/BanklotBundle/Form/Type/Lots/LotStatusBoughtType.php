<?php

namespace BanklotBundle\Form\Type\Lots;

use BanklotBundle\Form\Type\AbstractEntityType;
use BanklotBundle\Form\Type\Common\UploadedFileType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormBuilderInterface;

class LotStatusBoughtType extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('lot_status_bought', 'Lot');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('purchaseCost', 'number', array('label' => 'Стоимость покупки', 'scale' => 2))
            ->add('capitalization', 'integer', array('label' => 'Планируемый срок капитализации'))
            ->add('additionalExpenses', 'number', array('label' => 'Планируемые доп. затраты', 'scale' => 2))
            ->add('sale', 'number', array('label' => 'Планируемая стоимость продажи', 'scale' => 2))
            ->add('description', 'textarea', array('label' => 'Описание'))
            ->add('status', 'hidden', array('data' => 2))
            ->add('files', 'collection', array('label' => 'Документы', 'mapped' => false,
                'type' => new UploadedFileType(),
                'allow_add' => true,
                'delete_empty' => true,
                'options' => array('label' => false, 'required' => false)
            ))->add('responsible', 'entity', array('label' => 'Ответственный', 'required' => false,
                'class' => 'BanklotBundle\Entity\User',
                'choice_label' => 'nickname',
                'empty_value' => 'Выбрать пользователя',
                'query_builder' => function(EntityRepository $repository) {
                    return $repository->createQueryBuilder('u')
                        ->orderBy('u.nickname', 'ASC');
                }
            ));
    }
} 