<?php

namespace BanklotBundle\Form\Type\Lots;

use BanklotBundle\Form\Type\AbstractType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormBuilderInterface;

class LotsFilterType extends AbstractType
{
    public function __construct()
    {
        parent::__construct('lots_filter', self::METHOD_GET);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('category', 'entity', array(
            'label' => false,
            'required' => false,
            'class' => 'BanklotBundle\Entity\LotCategory',
            'choice_label' => 'name',
            'empty_value' => 'Все категории',
            'query_builder' => function(EntityRepository $repository) {
                return $repository->createQueryBuilder('c')
                    ->orderBy('c.name', 'ASC');
            }
        ))->add('search', 'text', array('label' => false, 'required' => false));
    }
} 