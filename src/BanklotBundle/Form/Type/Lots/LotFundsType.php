<?php

namespace BanklotBundle\Form\Type\Lots;

use BanklotBundle\Form\Type\AbstractEntityType;
use Symfony\Component\Form\FormBuilderInterface;

class LotFundsType extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('lot_funds', 'Money', 'lot_funds');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('sum', 'number', array('label' => 'Взнос', 'scale' => 2));
    }
} 