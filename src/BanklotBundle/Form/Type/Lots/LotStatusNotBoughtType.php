<?php

namespace BanklotBundle\Form\Type\Lots;

use BanklotBundle\Form\Type\AbstractEntityType;
use BanklotBundle\Form\Type\Common\UploadedFileType;
use Symfony\Component\Form\FormBuilderInterface;

class LotStatusNotBoughtType extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('lot_status_not_bought', 'Lot');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('additionalExpenses', 'number', array('label' => 'Затраты', 'scale' => 2))
            ->add('description', 'textarea', array('label' => 'Описание'))
            ->add('status', 'hidden', array('data' => 3))
            ->add('files', 'collection', array('label' => 'Документы', 'mapped' => false,
                'type' => new UploadedFileType(),
                'allow_add' => true,
                'delete_empty' => true,
                'options' => array('label' => false, 'required' => false)
            ));
    }
} 