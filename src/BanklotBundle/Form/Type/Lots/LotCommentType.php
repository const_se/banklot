<?php

namespace BanklotBundle\Form\Type\Lots;

use BanklotBundle\Form\Type\AbstractEntityType;
use Symfony\Component\Form\FormBuilderInterface;

class LotCommentType extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('comment', 'LotComment');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text', 'textarea', array('label' => 'Ваш комментарий'));
    }
} 