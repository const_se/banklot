<?php

namespace BanklotBundle\Form\Type\Lots;

use BanklotBundle\Entity\Lot;
use BanklotBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class LotStatusType extends AbstractType
{
    protected $status;

    public function __construct($status)
    {
        parent::__construct('lot_status');
        $this->status = $status;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $statuses = array();

        switch ($this->status) {
            case 0: $statuses = array(1 => 'На покупке'); break;
            case 1: $statuses = array(2 => 'Куплен', 3 => 'Не куплен'); break;
            case 2: $statuses = array(4 => 'Капитализирован'); break;
        }

        $builder->add('status', 'choice', array('label' => 'Статус лота',
            'choices' => $statuses,
            'empty_value' => 'Выберите статус'
        ));
    }
} 