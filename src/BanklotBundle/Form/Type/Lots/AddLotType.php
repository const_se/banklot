<?php

namespace BanklotBundle\Form\Type\Lots;

use BanklotBundle\Form\Type\AbstractEntityType;
use BanklotBundle\Form\Type\Common\UploadedFileType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormBuilderInterface;

class AddLotType extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('add_lot', 'Lot', 'add_lot');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $today = new \DateTime();
        $date = clone $today;
        $date->add(new \DateInterval('P3Y'));
        $builder->add('name', 'text', array('label' => 'Название лота'))
            ->add('category', 'entity', array('label' => false,
                'class' => 'BanklotBundle\Entity\LotCategory',
                'choice_label' => 'name',
                'empty_value' => 'Выберите категорию',
                'query_builder' => function(EntityRepository $repository) {
                    return $repository->createQueryBuilder('lc')
                        ->orderBy('lc.name', 'ASC');
                }
            ))->add('private', 'hidden', array('data' => '0'))
            ->add('description', 'textarea', array('label' => 'Описание лота', 'required' => false))
            ->add('url', 'url', array('label' => 'Ссылка на лот'))
            ->add('initialDate', 'date', array('label' => 'Дата подачи заявки', 'years' => range($today->format('Y'), $date->format('Y'))))
            ->add('participants', 'text', array('label' => 'Список пользователей', 'mapped' => false, 'required' => false))
            ->add('initialCost', 'money', array('label' => 'Начальная стоимость',
                'currency' => false
            ))->add('purchaseCost', 'money', array('label' => 'Стоимость покупки',
                'currency' => false
            ))->add('additionalExpenses', 'money', array('label' => 'Доп. затраты',
                'currency' => false
            ))->add('marketCost', 'money', array('label' => 'Рыночная стоимость',
                'currency' => false
            ))->add('ownFunds', 'money', array('label' => 'Собственные инвестиции', 'mapped' => false,
                'currency' => false,
                'data' => 0.0
            ))->add('capitalization', 'number', array('label' => 'Срок капитализации',
                'scale' => 0
            ))->add('files', 'collection', array('label' => 'Документы',
                'type' => new UploadedFileType(),
                'allow_add' => true,
                'delete_empty' => true,
                'options' => array('label' => false, 'required' => false)
            ));
    }
} 