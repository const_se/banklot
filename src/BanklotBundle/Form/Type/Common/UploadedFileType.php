<?php

namespace BanklotBundle\Form\Type\Common;

use BanklotBundle\Form\Type\AbstractEntityType;
use Symfony\Component\Form\FormBuilderInterface;

class UploadedFileType extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('uploaded_file', 'UploadedFile');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', 'file', array('label' => false));
    }
} 