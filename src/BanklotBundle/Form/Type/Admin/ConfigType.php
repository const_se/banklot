<?php

namespace BanklotBundle\Form\Type\Admin;

use BanklotBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ConfigType extends AbstractType
{
    public function __construct()
    {
        parent::__construct('config');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('allowed_balance', 'number', array('label' => 'Допустимый баланс доступа', 'scale' => 2))
            ->add('allowed_certificate_balance', 'number', array('label' => 'Допустимый баланс доступа для учеников', 'scale' => 2))
            ->add('withdraw_percent', 'number', array('label' => 'Процент комиссии за вывод средств', 'scale' => 0))
            ->add('withdraw_certificate_percent', 'number', array('label' => 'Процент комиссии за вывод средств для учеников', 'scale' => 0))
            ->add('capitalized_percent', 'number', array('label' => 'Процент комиссии с чистой прибыли', 'scale' => 0))
            ->add('capitalized_certificate_percent', 'number', array('label' => 'Процент комиссии с чистой прибыли для учеников', 'scale' => 0))
            ->add('own_percent', 'number', array('label' => 'Процент авторского вознаграждения', 'scale' => 0))
            ->add('responsible_percent', 'number', array('label' => 'Процент вознаграждения исполнителю', 'scale' => 0))
            ->add('invite_percent', 'number', array('label' => 'Процент с чистой прибыли тому, кто пригласил', 'scale' => 0));
    }
}