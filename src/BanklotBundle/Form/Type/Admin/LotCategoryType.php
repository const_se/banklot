<?php

namespace BanklotBundle\Form\Type\Admin;

use BanklotBundle\Form\Type\AbstractEntityType;
use Symfony\Component\Form\FormBuilderInterface;

class LotCategoryType extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('category', 'LotCategory');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array('label' => 'Наименование'));
    }
}