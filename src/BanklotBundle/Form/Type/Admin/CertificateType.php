<?php

namespace BanklotBundle\Form\Type\Admin;

use BanklotBundle\Form\Type\AbstractEntityType;
use Symfony\Component\Form\FormBuilderInterface;

class CertificateType extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('certificate', 'Certificate');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array('label' => 'Фамилия, имя, отчество'))
            ->add('number', 'text', array('label' => 'Номер сертификата'));
    }
}