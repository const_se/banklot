<?php

namespace BanklotBundle\Form\Type\Admin;

use BanklotBundle\Entity\Money;
use BanklotBundle\Form\Type\AbstractEntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormBuilderInterface;

class MoneyType extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('money', 'Money');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('user', 'entity', array('label' => 'Пользователь',
                'class' => 'BanklotBundle\Entity\User',
                'choice_label' => 'nickname',
                'empty_value' => 'Выберите пользователя',
                'query_builder' => function(EntityRepository $repository) {
                    return $repository->createQueryBuilder('u')
                        ->orderBy('u.nickname', 'ASC');
                }
            ))->add('sum', 'number', array('label' => 'Сумма', 'scale' => 2))
            ->add('type', 'choice', array('label' => 'Тип движения',
                'choices' => array(
                    Money::RECHARGE => 'Пополнение баланса',
                    Money::WITHDRAW => 'Вывод из системы'
                ),
                'empty_value' => 'Выберите тип'
            ))->add('confirmed', 'checkbox', array('label' => 'Подтвердить', 'required' => false, 'data' => true))
            ->add('comment', 'textarea', array('label' => 'Комментарий', 'required' => false));
    }
} 