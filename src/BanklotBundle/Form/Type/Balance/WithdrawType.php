<?php

namespace BanklotBundle\Form\Type\Balance;

use BanklotBundle\Form\Type\AbstractEntityType;
use Symfony\Component\Form\FormBuilderInterface;

class WithdrawType extends AbstractEntityType
{
    public function __construct()
    {
        parent::__construct('recharge', 'Money', 'recharge');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('sum', 'number', array('label' => false, 'scale' => 2));
    }
} 