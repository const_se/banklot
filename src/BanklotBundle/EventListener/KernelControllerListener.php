<?php

namespace BanklotBundle\EventListener;

use BanklotBundle\Controller\InitializableController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class KernelControllerListener
{
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();
        $controller = $controller[0];

        if ($controller instanceof InitializableController) $controller->initialize();
    }
} 