<?php

namespace BanklotBundle\DataFixtures\ORM;

use BanklotBundle\Entity\AdminUser;
use BanklotBundle\Entity\LotCategory;
use BanklotBundle\Entity\Role;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DataFixtures implements FixtureInterface, ContainerAwareInterface
{
    /** @var ContainerInterface */
    protected $container;

    public function load(ObjectManager $manager)
    {
        $role = new Role();
        $role->setName('Пользователь')
            ->setRole(Role::USER);
        $manager->persist($role);
        $manager->flush();

        $role = new Role();
        $role->setName('Администратор')
            ->setRole(Role::ADMIN);
        $manager->persist($role);
        $manager->flush();

        $admin = new AdminUser();
        /** @var \Symfony\Component\Security\Core\Encoder\UserPasswordEncoder $encoder */
        $encoder = $this->container->get('security.password_encoder');
        $admin->setSalt(AdminUser::generateSalt())
            ->setPassword($encoder->encodePassword($admin, 'admin'))
            ->setUsername('admin');
        $manager->persist($admin);
        $manager->flush();

        /** @var \BanklotBundle\Entity\Repository\ConfigRepository $configRepository */
        $configRepository = $manager->getRepository('BanklotBundle:Config');
        $manager->persist($configRepository->set('allowed_balance', 50000));
        $manager->persist($configRepository->set('allowed_certificate_balance', 30000));
        $manager->flush();

        $categories = array(
            'Недвижимость',
            'Авто',
            'Спецтехника',
            'ТМЦ',
            'Дебиторская задолженность',
            'Ценные бумаги',
            'Станки и оборудование',
            'Металлолом',
            'Другое'
        );

        foreach ($categories as $name) {
            $category = new LotCategory();
            $category->setName($name);
            $manager->persist($category);
        }

        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
} 