<?php

namespace BanklotBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Money
 * @package BanklotBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name = "moneys")
 */
class Money extends AbstractEntity
{
    const RECHARGE = 0;
    const TO_LOT = 2;
    const FROM_LOT_CAPITALIZE = 3;
    const FROM_LOT_NOT_BOUGHT = 4;
    const OWNER_BONUS = 5;
    const RESPONSIBLE_BONUS = 6;
    const WITHDRAW = 1;

    /**
     * @var string
     * @ORM\Column(name = "comment", type = "text", nullable = true)
     */
    protected $comment;

    /**
     * @var bool
     * @ORM\Column(name = "confirmed", type = "boolean")
     */
    protected $confirmed;

    /**
     * @var Lot
     * @ORM\ManyToOne(targetEntity = "Lot", inversedBy = "moneys")
     * @ORM\JoinColumn(name = "lot_id", referencedColumnName = "id", nullable = true)
     */
    protected $lot;

    /**
     * @var float
     * @ORM\Column(name = "money_sum", type = "float")
     * @Assert\NotBlank(groups = {"recharge", "lot_funds"}, message = "Сумма взноса не может быть пустой")
     * @Assert\GreaterThan(groups = {"recharge", "lot_funds"}, value = "0", message = "Сумма не может быть нулевой или отрицательной")
     */
    protected $sum;

    /**
     * @var int
     * @ORM\Column(name = "money_type", type = "integer")
     */
    protected $type;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity = "User", inversedBy = "moneys")
     * @ORM\JoinColumn(name = "user_id", referencedColumnName = "id", nullable = true)
     */
    protected $user;

    public function __construct()
    {
        parent::__construct();
        $this->confirmed = false;
        $this->sum = 0.00;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return Lot
     */
    public function getLot()
    {
        return $this->lot;
    }

    /**
     * @return float
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function isConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * @param string $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @param bool $confirmed
     * @return $this
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;

        return $this;
    }

    /**
     * @param Lot $lot
     * @return $this
     */
    public function setLot(Lot $lot)
    {
        $this->lot = $lot;

        return $this;
    }

    /**
     * @param float $sum
     * @return $this
     */
    public function setSum($sum)
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * @param int $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }
}