<?php

namespace BanklotBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * Class User
 * @package BanklotBundle\Entity
 * @ORM\Entity(repositoryClass = "BanklotBundle\Entity\Repository\UserRepository")
 * @ORM\Table(name = "users")
 */
class User extends AbstractEntity implements AdvancedUserInterface, \Serializable
{
    const REGISTERED = 2;
    const REGISTRATION_1 = 0;
    const REGISTRATION_2 = 1;

    /**
     * @var string
     * @ORM\Column(name = "address", type = "text", nullable = true)
     */
    protected $address;

    /**
     * @var float
     * @ORM\Column(name = "balance", type = "float")
     */
    protected $balance;

    /**
     * @var string
     * @ORM\Column(name = "bank_account", type = "string", nullable = true)
     */
    protected $bankAccount;

    /**
     * @var string
     * @ORM\Column(name = "bank_bik", type = "string", nullable = true)
     */
    protected $bankBIK;

    /**
     * @var string
     * @ORM\Column(name = "certificate", type = "string", nullable = true)
     */
    protected $certificate;

    /**
     * @var string
     * @ORM\Column(name = "city", type = "string", nullable = true)
     */
    protected $city;

    /**
     * @var ArrayCollection|LotComment[]
     * @ORM\OneToMany(targetEntity = "LotComment", mappedBy = "user", cascade = {"persist"})
     */
    protected $comments;

    /**
     * @var string
     * @ORM\Column(name = "country", type = "string", nullable = true)
     */
    protected $country;

    /**
     * @var \DateTime
     * @ORM\Column(name = "date_of_birth", type = "date", nullable = true)
     */
    protected $dateOfBirth;

    /**
     * @var bool
     * @ORM\Column(name = "enabled", type = "boolean")
     */
    protected $enabled;

    /**
     * @var ArrayCollection|UploadedFile[]
     * @ORM\ManyToMany(targetEntity = "UploadedFile")
     * @ORM\JoinTable(name = "user_files",
     *   joinColumns = {@ORM\JoinColumn(name = "user_id", referencedColumnName = "id")},
     *   inverseJoinColumns = {@ORM\JoinColumn(name = "file_id", referencedColumnName = "id")}
     * )
     */
    protected $files;

    /**
     * @var string
     * @ORM\Column(name = "inn", type = "string", nullable = true)
     */
    protected $INN;

    /**
     * @var ArrayCollection|User[]
     * @ORM\OneToMany(targetEntity = "User", mappedBy = "invitedBy", cascade = {"persist"})
     */
    protected $invited;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity = "User", inversedBy = "invited")
     * @ORM\JoinColumn(name = "invited_by_id", referencedColumnName = "id", nullable = true)
     */
    protected $invitedBy;

    /**
     * @var ArrayCollection|LotInvite[]
     * @ORM\OneToMany(targetEntity = "LotInvite", mappedBy = "user", cascade = {"persist"})
     */
    protected $invites;

    /**
     * @var bool
     * @ORM\Column(name = "locked", type = "boolean")
     */
    protected $locked;

    /**
     * @var ArrayCollection|Lot[]
     * @ORM\ManyToMany(targetEntity = "Lot", mappedBy = "participants")
     */
    protected $lots;

    /**
     * @var ArrayCollection|Money[]
     * @ORM\OneToMany(targetEntity = "Money", mappedBy = "user")
     */
    protected $moneys;

    /**
     * @var string
     * @ORM\Column(name = "name", type = "string", nullable = true)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(name = "nickname", type = "string", unique = true, nullable = true)
     */
    protected $nickname;

    /**
     * @var ArrayCollection|Notice[]
     * @ORM\OneToMany(targetEntity = "Notice", mappedBy = "toUser", cascade = {"persist"})
     */
    protected $notices;

    /**
     * @var ArrayCollection|Lot[]
     * @ORM\OneToMany(targetEntity = "Lot", mappedBy = "owner", cascade = {"persist"})
     */
    protected $ownLots;

    /**
     * @var \DateTime
     * @ORM\Column(name = "passport_date_of_issue", type = "date", nullable = true)
     */
    protected $passportDateOfIssue;

    /**
     * @var string
     * @ORM\Column(name = "passport_issued_by", type = "text", nullable = true)
     */
    protected $passportIssuedBy;

    /**
     * @var string
     * @ORM\Column(name = "passport_number", type = "string", nullable = true)
     */
    protected $passportNumber;

    /**
     * @var string
     * @ORM\Column(name = "passport_series", type = "string", nullable = true)
     */
    protected $passportSeries;

    /**
     * @var string
     * @ORM\Column(name = "password", type = "string")
     */
    protected $password;

    /**
     * @var string
     * @ORM\Column(name = "phone", type = "string", nullable = true)
     */
    protected $phone;

    /**
     * @var string
     * @ORM\Column(name = "region", type = "string", nullable = true)
     */
    protected $region;

    /**
     * @var ArrayCollection|Role[]
     * @ORM\ManyToMany(targetEntity = "Role", inversedBy = "users")
     * @ORM\JoinTable(name = "user_roles",
     *   joinColumns = {@ORM\JoinColumn(name = "user_id", referencedColumnName = "id")},
     *   inverseJoinColumns = {@ORM\JoinColumn(name = "role_id", referencedColumnName = "id")}
     * )
     */
    protected $roles;

    /**
     * @var string
     * @ORM\Column(name = "salt", type = "string")
     */
    protected $salt;

    /**
     * @var int
     * @ORM\Column(name = "status", type = "integer")
     */
    protected $status;

    /**
     * @var string
     * @ORM\Column(name = "username", type = "string", unique = true)
     */
    protected $username;

    public function __construct()
    {
        parent::__construct();
        $this->balance = 0.00;
        $this->comments = new ArrayCollection();
        $this->enabled = true;
        $this->files = new ArrayCollection();
        $this->invited = new ArrayCollection();
        $this->invites = new ArrayCollection();
        $this->locked = false;
        $this->lots = new ArrayCollection();
        $this->moneys = new ArrayCollection();
        $this->notices = new ArrayCollection();
        $this->ownLots = new ArrayCollection();
        $this->roles = new ArrayCollection();
        $this->status = self::REGISTRATION_1;
    }

    /**
     * @param LotComment $comment
     * @return $this
     */
    public function addComment(LotComment $comment)
    {
        if (!$this->comments->contains($comment)) $this->comments->add($comment);

        return $this;
    }

    /**
     * @param UploadedFile $file
     * @return $this
     */
    public function addFile(UploadedFile $file)
    {
        if (!$this->files->contains($file)) $this->files->add($file);

        return $this;
    }

    /**
     * @param LotInvite $invite
     * @return $this
     */
    public function addInvite(LotInvite $invite)
    {
        if (!$this->invites->contains($invite)) $this->invites->add($invite);

        return $this;
    }

    /**
     * @param User $invited
     * @return $this
     */
    public function addInvited(User $invited)
    {
        if (!$this->invited->contains($invited)) $this->invited->add($invited);

        return $this;
    }

    /**
     * @param Lot $lot
     * @return $this
     */
    public function addLot(Lot $lot)
    {
        if (!$this->lots->contains($lot)) $this->lots->add($lot);

        return $this;
    }

    /**
     * @param Money $money
     * @return $this
     */
    public function addMoney(Money $money)
    {
        if (!$this->moneys->contains($money)) $this->moneys->add($money);

        return $this;
    }

    /**
     * @param Notice $notice
     * @return $this
     */
    public function addNotice(Notice $notice)
    {
        if (!$this->notices->contains($notice)) $this->notices->add($notice);

        return $this;
    }

    /**
     * @param Lot $lot
     * @return $this
     */
    public function addOwnLot(Lot $lot)
    {
        if (!$this->ownLots->contains($lot)) $this->ownLots->add($lot);

        return $this;
    }

    /**
     * @param Role $role
     * @return $this
     */
    public function addRole(Role $role)
    {
        if (!$this->roles->contains($role)) $this->roles->add($role);

        return $this;
    }

    public function eraseCredentials()
    {
        return true;
    }

    public static function generateSalt()
    {
        $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $salt = '';

        foreach (range(1, 22) as $i) $salt .= $chars[mt_rand(0, strlen($chars) - 1)];

        return $salt;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return float
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @return string
     */
    public function getBankAccount()
    {
        return $this->bankAccount;
    }

    /**
     * @return string
     */
    public function getBankBIK()
    {
        return $this->bankBIK;
    }

    /**
     * @return string
     */
    public function getCertificate()
    {
        return $this->certificate;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return LotComment[]|ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return \DateTime
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @return UploadedFile[]|ArrayCollection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @return string
     */
    public function getINN()
    {
        return $this->INN;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getInvited()
    {
        return $this->invited;
    }

    /**
     * @return User
     */
    public function getInvitedBy()
    {
        return $this->invitedBy;
    }

    /**
     * @return LotInvite[]|ArrayCollection
     */
    public function getInvites()
    {
        return $this->invites;
    }

    /**
     * @return Lot[]|ArrayCollection
     */
    public function getLots()
    {
        return $this->lots;
    }

    /**
     * @return ArrayCollection|Money[]
     */
    public function getMoneys()
    {
        return $this->moneys;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @return ArrayCollection|Notice[]
     */
    public function getNotices()
    {
        return $this->notices;
    }

    /**
     * @return Lot[]|ArrayCollection
     */
    public function getOwnLots()
    {
        return $this->ownLots;
    }

    /**
     * @return \DateTime
     */
    public function getPassportDateOfIssue()
    {
        return $this->passportDateOfIssue;
    }

    /**
     * @return string
     */
    public function getPassportIssuedBy()
    {
        return $this->passportIssuedBy;
    }

    /**
     * @return string
     */
    public function getPassportNumber()
    {
        return $this->passportNumber;
    }

    /**
     * @return string
     */
    public function getPassportSeries()
    {
        return $this->passportSeries;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @return string
     */
    public function getRoleName()
    {
        foreach ($this->roles as $role)
            if ($role->getRole() == Role::ADMIN)
                return $role->getName();

        return $this->roles->first()->getName();
    }

    /**
     * @return array|\Symfony\Component\Security\Core\Role\Role[]
     */
    public function getRoles()
    {
        return $this->roles->toArray();
    }

    /**
     * @return Role[]|ArrayCollection
     */
    public function getRolesCollection()
    {
        return $this->roles;
    }

    /**
     * @return null|string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return !$this->locked;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return bool
     */
    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * @param LotComment $comment
     * @return bool\
     */
    public function hasComment(LotComment $comment)
    {
        return $this->comments->contains($comment);
    }

    /**
     * @param UploadedFile $file
     * @return bool
     */
    public function hasFile(UploadedFile $file)
    {
        return $this->files->contains($file);
    }

    /**
     * @param LotInvite $invite
     * @return bool
     */
    public function hasInvite(LotInvite $invite)
    {
        return $this->invites->contains($invite);
    }

    /**
     * @param User $invited
     * @return bool
     */
    public function hasInvited(User $invited)
    {
        return $this->invited->contains($invited);
    }

    /**
     * @param Lot $lot
     * @return bool
     */
    public function hasLot(Lot $lot)
    {
        return $this->lots->contains($lot);
    }

    /**
     * @param Money $money
     * @return bool
     */
    public function hasMoney(Money $money)
    {
        return $this->moneys->contains($money);
    }

    /**
     * @param Notice $notice
     * @return bool
     */
    public function hasNotice(Notice $notice)
    {
        return $this->notices->contains($notice);
    }

    /**
     * @param Lot $lot
     * @return bool
     */
    public function hasOwnLot(Lot $lot)
    {
        return $this->ownLots->contains($lot);
    }

    /**
     * @param Role $role
     * @return bool
     */
    public function hasRole(Role $role)
    {
        return $this->roles->contains($role);
    }

    /**
     * @param LotComment $comment
     * @return $this
     */
    public function removeComment(LotComment $comment)
    {
        $this->comments->removeElement($comment);

        return $this;
    }

    /**
     * @param UploadedFile $file
     * @return $this
     */
    public function removeFile(UploadedFile $file)
    {
        $this->files->removeElement($file);

        return $this;
    }

    /**
     * @param LotInvite $invite
     * @return $this
     */
    public function removeInvite(LotInvite $invite)
    {
        $this->invites->removeElement($invite);

        return $this;
    }

    /**
     * @param User $invited
     * @return $this
     */
    public function removeInvited(User $invited)
    {
        $this->invited->removeElement($invited);

        return $this;
    }

    /**
     * @param Lot $lot
     * @return $this
     */
    public function removeLot(Lot $lot)
    {
        $this->lots->removeElement($lot);

        return $this;
    }

    /**
     * @param Money $money
     * @return $this
     */
    public function removeMoney(Money $money)
    {
        $this->moneys->removeElement($money);

        return $this;
    }

    /**
     * @param Notice $notice
     * @return $this
     */
    public function removeNotice(Notice $notice)
    {
        $this->notices->removeElement($notice);

        return $this;
    }

    /**
     * @param Lot $lot
     * @return $this
     */
    public function removeOwnLot(Lot $lot)
    {
        $this->ownLots->removeElement($lot);

        return $this;
    }

    /**
     * @param Role $role
     * @return $this
     */
    public function removeRole(Role $role)
    {
        $this->roles->removeElement($role);

        return $this;
    }

    public function serialize()
    {
        return serialize(array(
            'id' => $this->id,
            'username' => $this->username
        ));
    }

    /**
     * @param string $address
     * @return $this
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @param float $balance
     * @return $this
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * @param string $bankAccount
     * @return $this
     */
    public function setBankAccount($bankAccount)
    {
        $this->bankAccount = $bankAccount;

        return $this;
    }

    /**
     * @param string $bankBIK
     * @return $this
     */
    public function setBankBIK($bankBIK)
    {
        $this->bankBIK = $bankBIK;

        return $this;
    }

    /**
     * @param string $certificate
     * @return $this
     */
    public function setCertificate($certificate)
    {
        $this->certificate = $certificate;

        return $this;
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @param string $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @param \DateTime $dateOfBirth
     * @return $this
     */
    public function setDateOfBirth(\DateTime $dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * @param bool $enabled
     * @return $this
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @param string $INN
     * @return $this
     */
    public function setINN($INN)
    {
        $this->INN = $INN;

        return $this;
    }

    /**
     * @param User $invitedBy
     * @return $this
     */
    public function setInvitedBy(User $invitedBy = null)
    {
        $this->invitedBy = $invitedBy;

        return $this;
    }

    /**
     * @param bool $locked
     * @return $this
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $nickname
     * @return $this
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * @param \DateTime $passportDateOfIssue
     * @return $this
     */
    public function setPassportDateOfIssue(\DateTime $passportDateOfIssue)
    {
        $this->passportDateOfIssue = $passportDateOfIssue;

        return $this;
    }

    /**
     * @param string $passportIssuedBy
     * @return $this
     */
    public function setPassportIssuedBy($passportIssuedBy)
    {
        $this->passportIssuedBy = $passportIssuedBy;

        return $this;
    }

    /**
     * @param $passportNumber
     * @return $this
     */
    public function setPassportNumber($passportNumber)
    {
        $this->passportNumber = $passportNumber;

        return $this;
    }

    /**
     * @param string $passportSeries
     * @return $this
     */
    public function setPassportSeries($passportSeries)
    {
        $this->passportSeries = $passportSeries;

        return $this;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @param string $region
     * @return $this
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @param string $salt
     * @return $this
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    public function unserialize($serialized)
    {
        $unserialized = unserialize($serialized);
        $this->id = $unserialized['id'];
        $this->username = $unserialized['username'];
    }
} 