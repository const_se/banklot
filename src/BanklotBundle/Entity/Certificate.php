<?php

namespace BanklotBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Certificate
 * @package BanklotBundle\Entity
 * @ORM\Entity(repositoryClass = "BanklotBundle\Entity\Repository\CertificateRepository")
 * @ORM\Table(name = "certificates")
 */
class Certificate extends AbstractEntity
{
    /**
     * @var string
     * @ORM\Column(name = "name", type = "string")
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(name = "number", type = "string")
     */
    protected $number;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $number
     * @return $this
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }
} 