<?php

namespace BanklotBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * Class Role
 * @package BanklotBundle\Entity
 * @ORM\Entity(repositoryClass = "BanklotBundle\Entity\Repository\RoleRepository")
 * @ORM\Table(name = "roles")
 */
class Role extends AbstractEntity implements RoleInterface
{
    const ADMIN = 'ROLE_ADMIN';
    const AUTHENTICATED_ANONYMOUSLY = 'IS_AUTHENTICATED_ANONYMOUSLY';
    const AUTHENTICATED_FULLY = 'IS_AUTHENTICATED_FULLY';
    const USER = 'ROLE_USER';

    /**
     * @var string
     * @ORM\Column(name = "name", type = "string")
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(name = "role", type = "string", unique = true)
     */
    protected $role;

    /**
     * @var ArrayCollection|User[]
     * @ORM\ManyToMany(targetEntity = "User", mappedBy = "roles")
     */
    protected $users;

    public function __construct()
    {
        parent::__construct();
        $this->users = new ArrayCollection();
    }

    /**
     * @param User $user
     * @return $this
     */
    public function addUser(User $user)
    {
        if (!$this->users->contains($user)) $this->users->add($user);

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function hasUser(User $user)
    {
        return $this->users->contains($user);
    }

    /**
     * @param User $user
     * @return $this
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);

        return $this;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }
} 