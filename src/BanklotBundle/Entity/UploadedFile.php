<?php

namespace BanklotBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class UploadedFile
 * @package BanklotBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name = "uploaded_files")
 */
class UploadedFile extends AbstractEntity
{
    /**
     * @var string
     * @ORM\Column(name = "extension", type = "string")
     */
    protected $extension;

    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    protected $file;

    /**
     * @var string
     * @ORM\Column(name = "path", type = "string")
     */
    protected $path;

    /**
     * @var string
     * @ORM\Column(name = "upload_path", type = "string")
     */
    protected $uploadPath;

    public function __construct()
    {
        parent::__construct();
        $this->uploadPath = 'uploads';
    }

    /**
     * @return null|string
     */
    public function getAbsolutePath()
    {
        return is_null($this->path) ? null : $this->getUploadRootDir() . '/' . $this->path;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return $this->uploadPath;
    }

    /**
     * @return null|string
     */
    public function getWebPath()
    {
        return is_null($this->path) ? null : $this->uploadPath . '/' . $this->path;
    }

    /**
     * @ORM\PreRemove
     */
    public function preRemove()
    {
        if (file_exists($this->getAbsolutePath())) unlink($this->getAbsolutePath());
    }

    /**
     * @param string $extension
     * @return $this
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return $this
     */
    public function setFile(\Symfony\Component\HttpFoundation\File\UploadedFile $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @param string $path
     * @return $this
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @param string $uploadPath
     * @return $this
     */
    public function setUploadPath($uploadPath)
    {
        $this->uploadPath = $uploadPath;

        return $this;
    }

    /**
     * @param null|string $uploadPath
     * @return \Symfony\Component\HttpFoundation\File\File
     */
    public function upload($uploadPath = null)
    {
        if (is_null($this->file)) return false;

        $file = $this->file;
        $this->file = null;
        $this->uploadPath = is_null($uploadPath) ? $this->uploadPath : $uploadPath;
        $extension = null; //$file->guessExtension();
//        $extension = is_null($extension) ? $file->guessClientExtension() : $extension;
        $extension = is_null($extension) ? $file->getClientOriginalExtension() : $extension;
        $this->extension = strtolower($extension);
        $name = basename($file->getClientOriginalName(), $file->getClientOriginalExtension());
        $path = $name . $extension;
        $i = 1;

        while (file_exists($this->getAbsolutePath())) {
            $path = $name . $i . '.' . $extension;
            $i++;
        }

        $this->path = $path;

        return $file->move($this->getUploadRootDir(), $this->path);
    }

    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../web/' . $this->uploadPath;
    }
} 