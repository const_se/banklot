<?php

namespace BanklotBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Notice
 * @package BanklotBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name = "notices")
 */
class Notice extends AbstractEntity
{
    const LOT_BOUGHT = 2;
    const LOT_CAPITALIZED = 4;
    const LOT_NOT_BOUGHT = 3;
    const LOT_PURCHASE = 1;
    const LOT_RAISING = 0;
    const TEXT = 7;
    const USER_INVITE = 5;
    const USER_JOIN = 6;

    /**
     * @var array
     * @ORM\Column(name = "options", type = "serializable", nullable = true)
     */
    protected $options;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity = "User", inversedBy = "notices")
     * @ORM\JoinColumn(name = "to_user_id", referencedColumnName = "id")
     */
    protected $toUser;

    /**
     * @var int
     * @ORM\Column(name = "notice_type", type = "integer")
     */
    protected $type;

    /**
     * @var bool
     * @ORM\Column(name = "viewed", type = "boolean")
     */
    protected $viewed;

    public function __construct()
    {
        parent::__construct();
        $this->options = array();
        $this->viewed = false;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @return User
     */
    public function getToUser()
    {
        return $this->toUser;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function isViewed()
    {
        return $this->viewed;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setToUser(User $user)
    {
        $this->toUser = $user;

        return $this;
    }

    /**
     * @param int $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param bool $viewed
     * @return $this
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;

        return $this;
    }
}