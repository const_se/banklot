<?php

namespace BanklotBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class CertificateRepository extends EntityRepository
{
    /**
     * @param string $name
     * @param string $number
     * @return \BanklotBundle\Entity\Certificate
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCertificate($name, $number)
    {
        return $this->createQueryBuilder('c')
            ->where('c.name = :name')
            ->andWhere('c.number = LOWER(:number)')
            ->setParameters(array('name' => $name, 'number' => $number))
            ->getQuery()
            ->getOneOrNullResult();
    }
} 