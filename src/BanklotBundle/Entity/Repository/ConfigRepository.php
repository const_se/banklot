<?php

namespace BanklotBundle\Entity\Repository;

use BanklotBundle\Entity\Config;
use Doctrine\ORM\EntityRepository;

class ConfigRepository extends EntityRepository
{
    /**
     * @param string $key
     * @param mixed $default
     * @return null|string
     */
    public function get($key, $default = null)
    {
        /** @var Config $config */
        $config = $this->findOneByKey($key);

        if (is_null($config)) return $default;

        return $config->getValue();
    }

    /**
     * @param string $key
     * @param string $value
     * @return Config
     */
    public function set($key, $value)
    {
        /** @var Config $config */
        $config = $this->findOneByKey($key);

        if (is_null($config)) {
            $config = new Config();
            $config->setKey($key);
        }

        $config->setValue($value);

        return $config;
    }
} 