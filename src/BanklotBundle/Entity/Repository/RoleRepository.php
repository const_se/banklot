<?php

namespace BanklotBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class RoleRepository extends EntityRepository
{
    /**
     * @param $role
     * @return \BanklotBundle\Entity\Role
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findRole($role)
    {
        return $this->createQueryBuilder('r')
            ->where('r.role = :role')
            ->setParameter('role', $role)
            ->getQuery()
            ->getOneOrNullResult();
    }
} 