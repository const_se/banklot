<?php

namespace BanklotBundle\Entity\Repository;

use BanklotBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    /**
     * @param User $user
     * @return int
     */
    public function findSamesCount(User $user)
    {
        $query = $this->createQueryBuilder('u')
            ->select('COUNT(u.id)');
        $id = $user->getId();
        $username = $user->getUsername();
        $nickname = $user->getNickname();

        if (empty($id))
            $query = $query->where('u.username = :username')
                ->setParameter('username', $username);
        elseif (empty($nickname))
            $query = $query->where('u.id <> :id')
                ->andWhere('u.username = :username')
                ->setParameters(array('id' => $id, 'username' => $username));
        else
            $query = $query->where('u.id <> :id')
                ->andWhere('u.username = :username OR u.nickname = :nickname')
                ->setParameters(array('id' => $id, 'username' => $username, 'nickname' => $nickname));

        return $query->getQuery()->getSingleScalarResult();
    }
} 