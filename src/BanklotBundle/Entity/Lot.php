<?php

namespace BanklotBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Lot
 * @package BanklotBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name = "lots")
 */
class Lot extends AbstractEntity
{
    const BOUGHT = 2;
    const CAPITALIZED = 3;
    const FUND_RAISING = 0;
    const NOT_BOUGHT = 4;
    const PURCHASE = 1;

    /**
     * @var float
     * @ORM\Column(name = "additional_expenses", type = "float")
     * @Assert\NotBlank(groups = {"add_lot"}, message = "Дополнительные затраты не могут быть пустыми")
     * @Assert\GreaterThanOrEqual(groups = {"add_lot"}, value = "0", message = "Сумма дополнительных затрат не может быть отрицательной")
     */
    protected $additionalExpenses;

    /**
     * @var int
     * @ORM\Column(name = "capitalization", type = "integer")
     * @Assert\NotBlank(groups = {"add_lot"}, message = "Срок капитализации не может быть пустым")
     * @Assert\GreaterThanOrEqual(groups = {"add_lot"}, value = "0", message = "Срок капитализации не может быть отрицательным")
     */
    protected $capitalization;

    /**
     * @var float
     * @ORM\Column(name = "capitalized_percent", type = "float", nullable = true)
     */
    protected $capitalizedPercent;

    /**
     * @var float
     * @ORM\Column(name = "capitalized_certificate_percent", type = "float", nullable = true)
     */
    protected $capitalizedCertificatePercent;

    /**
     * @var LotCategory
     * @ORM\ManyToOne(targetEntity = "LotCategory", inversedBy = "lots")
     * @ORM\JoinColumn(name = "category_id", referencedColumnName = "id")
     */
    protected $category;

    /**
     * @var ArrayCollection|LotComment[]
     * @ORM\OneToMany(targetEntity = "LotComment", mappedBy = "lot", cascade = {"persist"})
     */
    protected $comments;

    /**
     * @var string
     * @ORM\Column(name = "description", type = "text", nullable = true)
     */
    protected $description;

    /**
     * @var ArrayCollection|UploadedFile[]
     * @ORM\ManyToMany(targetEntity = "UploadedFile")
     * @ORM\JoinTable(name = "lot_files",
     *   joinColumns = {@ORM\JoinColumn(name = "lot_id", referencedColumnName = "id")},
     *   inverseJoinColumns = {@ORM\JoinColumn(name = "file_id", referencedColumnName = "id")}
     * )
     */
    protected $files;

    /**
     * @var float
     * @ORM\Column(name = "initial_cost", type = "float")
     * @Assert\NotBlank(groups = {"add_lot"}, message = "Начальная стомость не может быть пустой")
     * @Assert\GreaterThanOrEqual(groups = {"add_lot"}, value = "0", message = "Начальная стоимость не может быть отрицательной")
     */
    protected $initialCost;

    /**
     * @var \DateTime
     * @ORM\Column(name = "initial_date", type = "date")
     */
    protected $initialDate;

    /**
     * @var ArrayCollection|LotInvite[]
     * @ORM\OneToMany(targetEntity = "LotInvite", mappedBy = "lot", cascade = {"persist"})
     */
    protected $invites;

    /**
     * @var float
     * @ORM\Column(name = "market_cost", type = "float")
     * @Assert\NotBlank(groups = {"add_lot"}, message = "Рыночная стоимость не может быть пустой")
     * @Assert\GreaterThan(groups = {"add_lot"}, value = "0", message = "Рыночная стоимость не может быть отрицательной")
     */
    protected $marketCost;

    /**
     * @var ArrayCollection|Money[]
     * @ORM\OneToMany(targetEntity = "Money", mappedBy = "lot", cascade = {"persist"})
     */
    protected $moneys;

    /**
     * @var string
     * @ORM\Column(name = "name", type = "text")
     * @Assert\NotBlank(groups = {"add_lot"}, message = "Название лота не может быть пустым")
     */
    protected $name;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity = "User", inversedBy = "ownLots")
     * @ORM\JoinColumn(name = "owner_id", referencedColumnName = "id")
     */
    protected $owner;

    /**
     * @var float
     * @ORM\Column(name = "owner_percent", type = "float", nullable = true)
     */
    protected $ownerPercent;

    /**
     * @var User[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity = "User", inversedBy = "lots")
     * @ORM\JoinTable(name = "lot_participants",
     *   joinColumns = {@ORM\JoinColumn(name = "lot_id", referencedColumnName = "id")},
     *   inverseJoinColumns = {@ORM\JoinColumn(name = "participant_id", referencedColumnName = "id")}
     * )
     */
    protected $participants;

    /**
     * @var bool
     * @ORM\Column(name = "private", type = "boolean")
     */
    protected $private;

    /**
     * @var float
     * @ORM\Column(name = "purcase_cost", type = "float")
     * @assert\NotBlank(groups = {"add_lot"}, message = "Стоимость покупки не может быть пустой")
     * @Assert\GreaterThan(groups = {"add_lot"}, value = "0", message = "Стоимость покупки не может быть отрицательной")
     */
    protected $purchaseCost;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity = "User")
     * @ORM\JoinColumn(name = "responsible_id", referencedColumnName = "id", nullable = true)
     */
    protected $responsible;

    /**
     * @var float
     * @ORM\Column(name = "responsible_percent", type = "float", nullable = true)
     */
    protected $responsiblePercent;

    /**
     * @var float
     * @ORM\Column*(name = "sale", type = "float")
     */
    protected $sale;

    /**
     * @var int
     * @ORM\Column(name = "status", type = "integer")
     */
    protected $status;

    /**
     * @var string
     * @ORM\Column(name = "url", type = "text")
     */
    protected $url;

    public function __construct()
    {
        parent::__construct();
        $this->additionalExpenses = 0.00;
        $this->capitalization = 0;
        $this->comments = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->initialCost = 0.00;
        $this->initialDate = new \DateTime();
        $this->invites = new ArrayCollection();
        $this->marketCost = 0.00;
        $this->moneys = new ArrayCollection();
        $this->participants = new ArrayCollection();
        $this->private = false;
        $this->purchaseCost = 0.00;
        $this->sale = 0.00;
        $this->status = self::FUND_RAISING;
    }

    /**
     * @param LotComment $comment
     * @return $this
     */
    public function addComment(LotComment $comment)
    {
        if (!$this->comments->contains($comment)) $this->comments->add($comment);

        return $this;
    }

    /**
     * @param UploadedFile $file
     * @return $this
     */
    public function addFile(UploadedFile $file)
    {
        if (!$this->files->contains($file)) $this->files->add($file);

        return $this;
    }

    /**
     * @param LotInvite $invite
     * @return $this
     */
    public function addInvite(LotInvite $invite)
    {
        if (!$this->invites->contains($invite)) $this->files->add($invite);

        return $this;
    }

    /**
     * @param Money $money
     * @return $this
     */
    public function addMoney(Money $money)
    {
        if (!$this->moneys->contains($money)) $this->moneys->add($money);

        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function addParticipant(User $user)
    {
        if (!$this->participants->contains($user)) $this->participants->add($user);

        return $this;
    }

    /**
     * @return float
     */
    public function getAdditionalExpenses()
    {
        return $this->additionalExpenses;
    }

    /**
     * @return int
     */
    public function getCapitalization()
    {
        return $this->capitalization;
    }

    /**
     * @return float
     */
    public function getCapitalizedPercent()
    {
        return $this->capitalizedPercent;
    }

    /**
     * @return float
     */
    public function getCapitalizedCertificatePercent()
    {
        return $this->capitalizedCertificatePercent;
    }

    /**
     * @return LotCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return LotComment[]|ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return UploadedFile[]|ArrayCollection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @return float
     */
    public function getInitialCost()
    {
        return $this->initialCost;
    }

    /**
     * @return \DateTime
     */
    public function getInitialDate()
    {
        return $this->initialDate;
    }

    /**
     * @return LotInvite[]|ArrayCollection
     */
    public function getInvites()
    {
        return $this->invites;
    }

    /**
     * @return float
     */
    public function getMarketCost()
    {
        return $this->marketCost;
    }

    /**
     * @return Money[]|ArrayCollection
     */
    public function getMoneys()
    {
        return $this->moneys;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @return float
     */
    public function getOwnerPercent()
    {
        return $this->ownerPercent;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * @return float
     */
    public function getPurchaseCost()
    {
        return $this->purchaseCost;
    }

    /**
     * @return User
     */
    public function getResponsible()
    {
        return $this->responsible;
    }

    /**
     * @return float
     */
    public function getResponsiblePercent()
    {
        return $this->responsiblePercent;
    }

    /**
     * @return float
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param LotComment $comment
     * @return bool
     */
    public function hasComment(LotComment $comment)
    {
        return $this->comments->contains($comment);
    }

    /**
     * @param UploadedFile $file
     * @return bool
     */
    public function hasFile(UploadedFile $file)
    {
        return $this->files->contains($file);
    }

    /**
     * @param LotInvite $invite
     * @return bool
     */
    public function hasInvite(LotInvite $invite)
    {
        return $this->invites->contains($invite);
    }

    /**
     * @param Money $money
     * @return bool
     */
    public function hasMoney(Money $money)
    {
        return $this->moneys->contains($money);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function hasParticipant(User $user)
    {
        return $this->participants->contains($user);
    }

    /**
     * @return bool
     */
    public function isPrivate()
    {
        return $this->private;
    }

    /**
     * @param LotComment $comment
     * @return $this
     */
    public function removeComment(LotComment $comment)
    {
        $this->comments->removeElement($comment);

        return $this;
    }

    /**
     * @param UploadedFile $file
     * @return $this
     */
    public function removeFile(UploadedFile $file)
    {
        $this->files->removeElement($file);

        return $this;
    }

    /**
     * @param LotInvite $invite
     * @return $this
     */
    public function removeInvite(LotInvite $invite)
    {
        $this->invites->removeElement($invite);

        return $this;
    }

    /**
     * @param Money $money
     * @return $this
     */
    public function removeMoney(Money $money)
    {
        $this->moneys->removeElement($money);

        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function removeParticipant(User $user)
    {
        $this->participants->removeElement($user);

        return $this;
    }

    /**
     * @param float $additionalExpenses
     * @return $this
     */
    public function setAdditionalExpenses($additionalExpenses)
    {
        $this->additionalExpenses = $additionalExpenses;

        return $this;
    }

    /**
     * @param int $capitalization
     * @return $this
     */
    public function setCapitalization($capitalization)
    {
        $this->capitalization = $capitalization;

        return $this;
    }

    /**
     * @param float $capitalizedPercent
     * @return $this
     */
    public function setCapitalizedPercent($capitalizedPercent)
    {
        $this->capitalizedPercent = $capitalizedPercent;

        return $this;
    }

    /**
     * @param float $capitalizedCertificatePercent
     * @return $this
     */
    public function setCapitalizedCertificatePercent($capitalizedCertificatePercent)
    {
        $this->capitalizedCertificatePercent = $capitalizedCertificatePercent;

        return $this;
    }

    /**
     * @param LotCategory $category
     * @return $this
     */
    public function setCategory(LotCategory $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param float $initialCost
     * @return $this
     */
    public function setInitialCost($initialCost)
    {
        $this->initialCost = $initialCost;

        return $this;
    }

    /**
     * @param \DateTime $initialDate
     * @return $this
     */
    public function setInitialDate(\DateTime $initialDate)
    {
        $this->initialDate = $initialDate;

        return $this;
    }

    /**
     * @param float $marketCost
     * @return $this
     */
    public function setMarketCost($marketCost)
    {
        $this->marketCost = $marketCost;

        return $this;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param User $owner
     * @return $this
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @param float $ownerPercent
     * @return $this
     */
    public function setOwnerPercent($ownerPercent)
    {
        $this->ownerPercent = $ownerPercent;

        return $this;
    }

    /**
     * @param bool $private
     * @return $this
     */
    public function setPrivate($private)
    {
        $this->private = $private;

        return $this;
    }

    /**
     * @param float $purchaseCost
     * @return $this
     */
    public function setPurchaseCost($purchaseCost)
    {
        $this->purchaseCost = $purchaseCost;

        return $this;
    }

    /**
     * @param User $responsible
     * @return $this
     */
    public function setResponsible(User $responsible = null)
    {
        $this->responsible = $responsible;

        return $this;
    }

    /**
     * @param float $responsiblePercent
     * @return $this
     */
    public function setResponsiblePercent($responsiblePercent)
    {
        $this->responsiblePercent = $responsiblePercent;

        return $this;
    }

    /**
     * @param float $sale
     * @return $this
     */
    public function setSale($sale)
    {
        $this->sale = $sale;

        return $this;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }
} 