<?php

namespace BanklotBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class LotInvite
 * @package BanklotBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name = "lot_invites")
 */
class LotInvite extends AbstractEntity
{
    /**
     * @var bool
     * @ORM\Column(name = "confirmed", type = "boolean")
     */
    protected $confirmed;

    /**
     * @var Lot
     * @ORM\ManyToOne(targetEntity = "Lot", inversedBy = "invites")
     * @ORM\JoinColumn(name = "lot_id", referencedColumnName = "id")
     */
    protected $lot;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity = "User", inversedBy = "invites")
     * @ORM\JoinColumn(name = "user_id", referencedColumnName = "id")
     */
    protected $user;

    public function __construct()
    {
        parent::__construct();
        $this->confirmed = false;
    }

    /**
     * @return Lot
     */
    public function getLot()
    {
        return $this->lot;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function isConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * @param bool $confirmed
     * @return $this
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;

        return $this;
    }

    /**
     * @param Lot $lot
     * @return $this
     */
    public function setLot(Lot $lot)
    {
        $this->lot = $lot;

        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }
} 