<?php

namespace BanklotBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class LotCategory
 * @package BanklotBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name = "lot_categories")
 */
class LotCategory extends AbstractEntity
{
    /**
     * @var ArrayCollection|Lot[]
     * @ORM\OneToMany(targetEntity = "Lot", mappedBy = "category", cascade = {"persist"})
     */
    protected $lots;

    /**
     * @var string
     * @ORM\Column(name = "name", type = "string")
     */
    protected $name;

    public function __construct()
    {
        parent::__construct();
        $this->lots = new ArrayCollection();
    }

    /**
     * @param Lot $lot
     * @return $this
     */
    public function addLot(Lot $lot)
    {
        if (!$this->lots->contains($lot)) $this->lots->add($lot);

        return $this;
    }

    /**
     * @return Lot[]|ArrayCollection
     */
    public function getLots()
    {
        return $this->lots;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param Lot $lot
     * @return bool
     */
    public function hasLot(Lot $lot)
    {
        return $this->lots->contains($lot);
    }

    /**
     * @param Lot $lot
     * @return $this
     */
    public function removeLot(Lot $lot)
    {
        $this->lots->removeElement($lot);

        return $this;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
} 