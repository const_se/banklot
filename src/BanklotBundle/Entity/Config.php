<?php

namespace BanklotBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Config
 * @package BanklotBundle\Entity
 * @ORM\Entity(repositoryClass = "BanklotBundle\Entity\Repository\ConfigRepository")
 * @ORM\Table(name = "configs")
 */
class Config extends AbstractEntity
{
    /**
     * @var string
     * @ORM\Column(name = "config_key", type = "string", unique = true)
     */
    protected $key;

    /**
     * @var string
     * @ORM\Column(name = "config_value", type = "text", nullable = true)
     */
    protected $value;

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $key
     * @return $this
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
} 