<?php

namespace BanklotBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class LotComment
 * @package BanklotBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name = "lot_comments")
 */
class LotComment extends AbstractEntity
{
    /**
     * @var Lot
     * @ORM\ManyToOne(targetEntity = "Lot", inversedBy = "comments")
     * @ORM\JoinColumn(name = "lot_id", referencedColumnName = "id")
     */
    protected $lot;

    /**
     * @var string
     * @ORM\Column(name = "comment_text", type = "text")
     */
    protected $text;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity = "User", inversedBy = "comments")
     * @ORM\JoinColumn(name = "user_id", referencedColumnName = "id")
     */
    protected $user;

    /**
     * @return Lot
     */
    public function getLot()
    {
        return $this->lot;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param Lot $lot
     * @return $this
     */
    public function setLot(Lot $lot)
    {
        $this->lot = $lot;

        return $this;
    }

    /**
     * @param string $text
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }
} 