<?php

namespace BanklotBundle\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

class SerializableType extends Type
{
    const NAME = 'serializable';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (is_null($value)) return null;

        try {
            return serialize($value);
        }
        catch (\Exception $e) {
            throw ConversionException::conversionFailed($value, self::NAME);
        }
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (is_null($value)) return null;

        try {
            return unserialize($value);
        }
        catch (\Exception $e) {
            throw ConversionException::conversionFailed($value, self::NAME);
        }
    }

    public function getName()
    {
        return self::NAME;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getClobTypeDeclarationSQL($fieldDeclaration);
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}