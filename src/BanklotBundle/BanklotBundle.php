<?php

namespace BanklotBundle;

use BanklotBundle\Type\SerializableType;
use Doctrine\DBAL\Types\Type;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class BanklotBundle extends Bundle
{
    public function boot()
    {
        if (!Type::hasType(SerializableType::NAME))
            Type::addType(SerializableType::NAME, 'BanklotBundle\Type\SerializableType');

        /** @var \Doctrine\DBAL\Platforms\AbstractPlatform $platform */
        $platform = $this->container->get('database_connection')->getDatabasePlatform();

        if (!$platform->hasDoctrineTypeMappingFor(SerializableType::NAME))
            $platform->registerDoctrineTypeMapping(Type::TEXT, SerializableType::NAME);
    }
} 